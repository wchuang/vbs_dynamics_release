verbos = num2str(parms.plotVerbosity);
warning('off','MATLAB:Figure:SetPosition') % this is temporary, just to avoid warnings. The warning is turned on at the bottom of the script
if exist(['./figs' verbos], 'dir') ~= 7
    mkdir(['./figs' verbos])
end

calcForPlotsPolydisperse;

%% stacked plot of where all the organics are, massConc vs time
    xVar = 't_prod';
    xLabelStr = 'time (min)';
    fileSuffix = 't_stacked';
    plotCmd = 'area';

    figure
    C = [COA_prod Cd_prodSum Ct_prodSum Cvap_prod C_prec*sum(parms.y)];
    pl = area(t_prod, C);
    
    set(pl(1), 'FaceColor', 'green' );    
    set(pl(2), 'FaceColor', 'blue'  );
    set(pl(3), 'FaceColor', 'red'  );
    set(pl(4), 'FaceColor', [.5 .5 .5]);
    set(pl(5), 'FaceColor', 'cyan');
    
    setFigureParameters(paperSize,xLabelStr,'Mass (\mu g m^{-3})');
    set(gca,'layer','top')
    legend(fliplr(pl),{'Precursor Yield Potential','Bulk vapor','Vapors on Teflon','Particles on Walls','Bulk suspended'}, 'Location', 'SouthEast')
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix],'fig');
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix,'.pdf'],'pdf'); 
%% Mass concentration distributed by particle size at the beginning and end of the run
C_totsizes = Cs_prodsizes+Cseed_prodsizes;
figure    
pl = bar(Dp_prodsizes(1,:), Cs_prodsizes(1,:)+Cseed_prodsizes(1,:));
setFigureParameters(paperSize,'Diameter (nm)','Mass (\mu g m^{-3})');
set(gca,'layer','top')
xlim([Dp_prodsizes(1,1) max(Dp_prodsizes(end,:))]);
ylim([0 1.1*max(C_totsizes(:))]);
set(gca,'XScale','log');
set(pl,'barWidth',8)
saveas(gcf,['./figs/',verbos,'MassDistributionSeeds','.fig'],'fig');
saveas(gcf,['./figs/',verbos,'MassDistributionSeeds','.pdf'],'pdf'); 
% Plot the distribution at the peak of mass concentration
[rowindex,colindex] = find(C_totsizes==max(C_totsizes(:)));
[bincounts, ind] = histc(Dp_prodsizes(rowindex,:), Dp_prodsizes(1,:)*10^(4/64));
Cbinned = zeros(1,numSizes);
for i = 1:numSizes
    Cbinned(ind(i)) = Cs_prodsizes(rowindex,i)+Cseed_prodsizes(rowindex,i);
end
figure    
pl = bar(Dp_prodsizes(1,:)*10^(4/64), Cbinned);
setFigureParameters(paperSize,'Diameter (nm)','Mass (\mu g m^{-3})');
set(gca,'layer','top')
xlim([Dp_prodsizes(1,1) max(Dp_prodsizes(end,:))]);
ylim([0 1.1*max(C_totsizes(:))]);
set(gca,'XScale','log');
set(pl,'barWidth',8)
saveas(gcf,['./figs/',verbos,'MassDistributionPeak','.fig'],'fig');
saveas(gcf,['./figs/',verbos,'MassDistributionPeak','.pdf'],'pdf'); 
% Produce an avi of all the distributions
hold on
Dp_incSizes = [Dp_prodsizes(1,:), (Dp_prodsizes(1,end)*10.^((1:4)/64))];
hfig = figure;
for n = 1:rowindex+200
    [bincounts, ind] = histc(Dp_prodsizes(n,:),Dp_incSizes);
    Cbinned = zeros(1,numSizes+4);
    for i = 1:numSizes
        Cbinned(ind(i)) = Cs_prodsizes(n,i)+Cseed_prodsizes(n,i);
    end
    bar(Dp_incSizes, Cbinned);
    setFigureParameters(paperSize,'Diameter (nm)','Mass Concentration (\mu g m^{-3})');
    set(gca,'layer','top')
    xlim([Dp_prodsizes(1,1) max(Dp_prodsizes(end,:))]);
    ylim([0 1.1*max(C_totsizes(:))]);
    set(gca,'XScale','log');
    set(pl,'barWidth',8)
    M(n) = getframe(hfig);
end
v = VideoWriter(['./figs/',verbos,'/distributions.avi']);
open(v)
writeVideo(v,M)
close(v)
%% vapor condensation sink
% because we split the particle numbers up into their respective bins,
% we're combining them here to get the overall condensation sink for each
% volatility bin.
kc_prodbins = zeros(length(t_prod),numBins);
    for i = 1:numSizes
        kc_prodbins = kc_prodbins + Cscells{i};
    end
kc_ratio = kc_prodbins ./ parms.k_vt(1);
figure
pl = plot(t_prod, kc_ratio, 'g-');
    for v = (1:4)
        set(pl(v), 'Color', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'Color', [1 .9*v/8 .9]);
    end
    for v = (9:size(Cs_prodbins,2))
        set(pl(v), 'Color', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
set(pl,'LineWidth',3);
setFigureParameters(paperSize,'time (min)','CS_{particle}/CS_{wall}');
saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t' '.fig'],'fig');
saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t' '.pdf'],'pdf');
set(gca,'YScale','log');
saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t_log' '.fig'],'fig');
saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t_log' '.pdf'],'pdf');

%% mass contribution from each volatility bin over time
fileSuffix = 't_stacked';
figure 
pl = area(t_prod, Cs_prodbins);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = (9:size(Cs_prodbins,2))
        set(pl(v), 'FaceColor', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
set(gca,'layer','top')
setFigureParameters(paperSize,'time (min)','Mass Concentration (\mu g m^{-3})');
saveas(gcf,['./figs/',verbos,'/Cs_v_',fileSuffix,'_color'],'fig');
saveas(gcf,['./figs/',verbos,'/Cs_v_',fileSuffix,'_color.pdf'],'pdf');
%% plot mass contribution from seeds
hold off
figure
C = [Cseed_prod COA_prod];
pl = area(t_prod, C);
    set(pl(1), 'FaceColor', 'red'); %[1 0.5 0] );    
    set(pl(2), 'FaceColor', 'green'  );
legend(fliplr(pl),{'Bulk suspended organics','Bulk suspended seed'}, 'Location', 'NorthEast')
setFigureParameters(paperSize,'time (min)','Mass Concentration (\mu g m^{-3})');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,''],'fig');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,'.pdf'],'pdf'); 
%{
    for v = (1:4)
        set(pl(v+1), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v+1), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = (9:size(Cs_prodbins,2))
        set(pl(v+1), 'FaceColor', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,'_color'],'fig');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,'_color.pdf'],'pdf');
%}
    hold on
% Read the observations
fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Time.txt']);
C = textscan(fid,'%f','HeaderLines',1);
t_obs = C{1};
fclose(fid);
fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Mass.txt']);
C = textscan(fid,'%f','HeaderLines',1);
C_obs = C{1};
fclose(fid);
fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Seed Mass.txt']);
C = textscan(fid,'%f','HeaderLines',1);
seed_obs = C{1};
fclose(fid);
% Plot the observations
    pl2 = plot(t_obs,C_obs,'o'); % took out -0.3 nm diameter
set(pl2,'MarkerFaceColor',[.4 .4 .4]);
set(pl2,'MarkerEdgeColor','k');
set(pl2,'MarkerSize',10);
    pl2 = plot(t_obs,seed_obs,'.'); % took out -0.3 nm diameter
set(pl2,'MarkerFaceColor',[.2 .2 .2]);
set(pl2,'MarkerEdgeColor','k');
set(pl2,'MarkerSize',10);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/Cs_seeds_data_v_',fileSuffix,'_color'],'fig');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_data_v_',fileSuffix,'_color.pdf'],'pdf');
pause
