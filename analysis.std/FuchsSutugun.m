function beta = FuchsSutugun(Dp,lambda,alpha_p)

if nargin < 2, lambda = 80; end; % mean free path in nm
if nargin < 3, alpha_p = 1; end; % mass accommodation 

Kn = 2* lambda ./ (Dp);

beta = alpha_p .* Kn .* (1 + Kn) ./ ...
    (Kn.^2 + Kn + 0.283 * Kn * alpha_p + 0.75 * alpha_p);