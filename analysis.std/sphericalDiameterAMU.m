function Dp = sphericalDiameterAMU(MpAMU,rhop)

% Dp = sphericalDiameterAMU(MpAMU,rhop)
% 
% MpAMU particle mass in amu
% rhop  particle density in kg/m3 (ie near 1e3)

physicalConstants;

% Figure out the particle volume in m3
% Assume monodispersed and uniform concentration
Vp = (MpAMU*amutokg/rhop); 

% Now assume a sphere and volume mixing of seed and OA to figure out Dp
Dp = ((6/pi)*Vp).^(1/3) / nm2m; 
