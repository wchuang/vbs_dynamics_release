% Plot the yield distribution in a VBS framework
global parms
set(0,'DefaultFigureWindowStyle','docked')
paperSize = [9 5];

verbos = num2str(parms.plotVerbosity);

if exist(['./figs/' verbos], 'dir') ~= 7
    mkdir(['./figs/' verbos])
end
if exist(['./figs/' verbos '/distribution'], 'dir') ~= 7
    mkdir(['./figs/' verbos '/distribution'])
end  

%% plot the original yields in the VBS
figure
setFigureParameters([6.5,3],'log_{10} C^* (\mu g m^{-3})','mass yield');
br = bar(log10(parms.Co), parms.origy, 'g');
set(br(1),'LineWidth',2);
%
height = max(parms.y)*1.1;%
%
grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*5/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-7.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-6.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*7/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-5.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*8/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
%
LVOC  = rectangle('Position',[-4.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*5/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-3.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-2.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*7/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');
LVOC  = rectangle('Position',[-1.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*8/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
%
SVOC  = rectangle('Position',[-0.5, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, height+.005, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, height+.005, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.25, height+.005, 'SVOC','HorizontalAlignment','center','FontSize',16);
%
setFigureParameters(paperSize,{'log_{10} C^* (\mu g m^{-3})',['T=' num2str(parms.T) ' K']},'mass yield');
xlim([-9 1]);
ylim([0 max(parms.y)*1.1]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/2_VBS_CLOUD_orig.pdf'],'pdf');

%% plot scaled yields in the VBS
figure
setFigureParameters([6.5,3],'log_{10} C^* (\mu g m^{-3})','mass yield');
br = bar(log10(parms.Co), [parms.origy, parms.y - parms.origy], 'stack');
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
set(br(2),'FaceColor',[.75 1 .75]);
set(br(2),'LineWidth',2);
%
height = max(parms.y)*1.1;%
%
grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*5/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-7.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-6.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*7/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-5.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*8/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
%
LVOC  = rectangle('Position',[-4.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*5/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-3.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-2.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*7/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');
LVOC  = rectangle('Position',[-1.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*8/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, height+.005, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, height+.005, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.25, height+.005, 'SVOC','HorizontalAlignment','center','FontSize',16);
%
setFigureParameters(paperSize,{'log_{10} C^* (\mu g m^{-3})',['T=' num2str(parms.T) ' K']},'mass yield');
xlim([-9 1]);
ylim([0 max(parms.y)*1.1]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/2_VBS_CLOUD_corrected.pdf'],'pdf');

%% molar yield plots
parms.ymolar = parms.y * 168 ./ parms.Morg;
figure
br = bar(log10(parms.Co),[parms.ymolar],'stack');
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
setFigureParameters(paperSize,'log_{10} C^* (\mu g m^{-3})','molar yield');
%
height = .1;%
%
grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*5/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-7.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-6.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*7/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-5.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*8/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
%
LVOC  = rectangle('Position',[-4.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*5/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-3.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-2.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*7/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');
LVOC  = rectangle('Position',[-1.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*8/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
%
SVOC  = rectangle('Position',[-0.5, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, height+.005, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, height+.005, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.25, height+.005, 'SVOC','HorizontalAlignment','center','FontSize',16);
%
setFigureParameters(paperSize,{'log_{10} C^* (\mu g m^{-3})',['T=' num2str(parms.T) ' K']},'molar yield');
xlim([-9 1]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/2_VBS_CLOUDmolar_corrected.pdf'],'pdf');
