function simpleVBSDynamics( )
% This file sets the framework of organic reservoirs and volatility bins to
% pass into the ode, and then assigns the results from the ode. It then
% calls the plotting function.

set(0,'DefaultFigureWindowStyle','docked')

global parms

paperSize = [9 5];

physicalConstants;

numBins  = length(parms.y);
numSizes = length(parms.DpSeed);
Crun(1:numBins) = 0; % initialize vapor concentrations

%% vapor buildup, only if startTime < 0
if parms.timeStart < 0
    vaporBuildup;
    Crun = Cinit(end,:)'; % set the dynamic run to vapor concentrations from buildup
    tSpan = (.05:.05:parms.timeEnd);
else
    tSpan = (parms.timeStart:.05:parms.timeEnd);
end

%% Condensation step
    Cseed = 1/6*pi*(parms.DpSeed.*nm2m).^3 .* (parms.Np.*cm3tom3) * parms.rhoSeed * kgm3tougm3;
    Npw = zeros(size(parms.Np)); %number of particles on wall
    Cseedw = 1/6*pi*(parms.DpSeed.*nm2m).^3 .* (Npw.*cm3tom3) * parms.rhoSeed * kgm3tougm3;
    Crun(numBins +(1:numBins*(2*numSizes+1))) = 0; % organic vapor, suspended, teflon, deposited
    Crun(numBins*(2*numSizes+2) +(1:numSizes))                                     = Cseed';   % suspended seed mass during production
    Crun(numBins*(2*numSizes+2) +numSizes +(1:numSizes))                           = Cseedw'; % Initial seed mass on the wall
    Crun(numBins*(2*numSizes+2) +2*numSizes+(1:length(parms.Np)))                  = parms.Np';      % suspended particle number during production
    Crun(numBins*(2*numSizes+2) +2*numSizes+length(parms.Np)+(1:length(parms.Np))) = Npw'; % Initial particle number on wall
    Crun(numBins*(2*numSizes+2) +2*numSizes+2*length(parms.Np)+1)                  = parms.AP; % ug/m3
    Crun(numBins*(2*numSizes+2) +2*numSizes+2*length(parms.Np)+2)                  = 0; % ug/m3, total reacted precursor

    options = odeset('NonNegative',ones(size(Crun)),'RelTol',parms.relTol,'AbsTol',parms.absTol);
    [t_prod Cprod] = ode15s(@dynVBS,tSpan,Crun,options);

    Cv_prod =       Cprod(:, 1:numBins);  % bulk vapor
    Cs_prod =       Cprod(:, numBins + (1:numBins*numSizes));  % suspended particles during dilution
    Ct_prod =       Cprod(:, numBins*(numSizes+1) +(1:numBins));  % vapors on teflon
    Cd_prod =       Cprod(:, numBins*(numSizes+2) +(1:numBins*numSizes));  % deposited particles
    Cseed_prodsizes =    Cprod(:, numBins*(2*numSizes+2)+(1:numSizes)); % suspended seed mass
    Cseedw_prodsizes =   Cprod(:, numBins*(2*numSizes+2)+  numSizes +(1:numSizes)); 
    Np_prodsizes =       Cprod(:, numBins*(2*numSizes+2)+2*numSizes +(1:numSizes)); % suspended particle number 
    Npw_prodsizes =      Cprod(:, numBins*(2*numSizes+2)+3*numSizes +(1:numSizes)); 
    C_prec =        Cprod(:, numBins*(2*numSizes+2)+4*numSizes +1);
    C_precReacted = Cprod(:, numBins*(2*numSizes+2)+4*numSizes +2); % ug/m3 total reacted precursor

    %reshape if numSizes is more than 1

    Cscells = mat2cell(Cs_prod, length(t_prod),repmat(numBins,numSizes,1));
    Cdcells = mat2cell(Cd_prod, length(t_prod),repmat(numBins,numSizes,1));
    Cs_prodbins = zeros(length(t_prod),numBins);
    Cd_prodbins = zeros(length(t_prod),numBins);
    Cs_prodsizes = zeros(length(t_prod),numSizes);
    Cd_prodsizes = zeros(length(t_prod),numSizes);       
    for i = 1:numSizes
        Cs_prodbins = Cs_prodbins + Cscells{i};
        Cd_prodbins = Cd_prodbins + Cdcells{i};
        Cs_prodsizes(:,i) = sum(Cscells{i},2);
        Cd_prodsizes(:,i) = sum(Cdcells{i},2);
    end        
    COA_prod = sum(Cs_prodbins,2);    % sum of each row
    Cvap_prod = sum(Cv_prod,2);   % sum of each row for vapors
    Cd_prodSum = sum(Cd_prodbins,2);  % sum of deposited particles
    Ct_prodSum = sum(Ct_prod,2);  % sum of vapor on Teflon
    Cseed_prod = sum(Cseed_prodsizes,2); % sum of bulk seed concentrations
    Cseedw_prod = sum(Cseedw_prodsizes,2); % sum of wall-lost seed concentrations
    Np_prod = sum(Np_prodsizes,2); % sum of particle number concentration
    Npw_prod = sum(Npw_prodsizes,2); % sum of particle number on walls

    C_vapELVOCprod = sum(Cv_prod,2); % sum of all ELVOCs
    C_vapELVOCmlcprod = C_vapELVOCprod/250*6e23/1e12; % is called in vaporPlot.m

    %vaporPlot;
    %particlePlot;
    %totalPlot;

    % make a bunch of other plots
    %mainPlots;
    %summaryPlots;
    if numSizes == 1
        testPlot;
    elseif numSizes > 1
        testPlotPolydisperse;
    end
end

