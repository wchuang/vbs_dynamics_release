% script PlotVBSVec2
% This produces figures in the ELVOC/LVOC color scheme
figure
    for v = (1:4)
        pl = eval([plotCmd,'(',xVarName,'(plotSel),',yVarName,'(plotSel,v),''g-'')']);
        set(pl, 'Color', [.9 .9 .9]*(v+4)/8);
        set(pl,'LineWidth',3);
        hold on
    end
    for v = (5:7) % 8 goes here if including logC*=-1
        pl = eval([plotCmd,'(',xVarName,'(plotSel),',yVarName,'(plotSel,v),''g-'')']);
        set(pl, 'Color', [1 .9*v/8 .9]);
        set(pl,'LineWidth',3);
    end
    %{
    for v = (9:size(Cs_prodbins,2))
        pl = eval([plotCmd,'(',xVarName,'(plotSel),',yVarName,'(plotSel,v),''g-'')']);
        set(pl, 'Color', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
        set(pl,'LineWidth',3);
    end 
    %}