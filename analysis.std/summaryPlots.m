% Calculate the quantities for plotting
calcForPlots;
%%%%%
% Condensation sink plot

xVarName = 't_prod';
yVarName = 'kc_prod';
plotCmd = 'plot';
plotVBSVec;
setFigureParameters(paperSize,'time (min)','Average condensation sink (min^{-1})');
saveas(gcf,['figs/condSink_v_t' '.pdf'],'pdf');

pause;

%%%%%%%%%%%%%%%%%%%%%%
%
% Diameter plot

figure
pl = semilogy(t_prod,Dp_prod,'g-');
set(pl,'Color',[0,1,0]);
set(pl,'LineWidth',1.5);

hold

% Here are the observations -- better have been read!
pl2 = plot(parms.t_obs,parms.Dm_obs-0.3,'o');
set(pl2,'MarkerFaceColor',[.4 .4 .4]);
set(pl2,'MarkerEdgeColor','k');
set(pl2,'MarkerSize',10);

uistack(pl,'top');

setFigureParameters(paperSize,'time (min)','Particle Diameter (nm)');
saveas(gcf,'figs/diameter_v_t_log.pdf','pdf');

set(gca,'YScale','lin');
saveas(gcf,'figs/diameter_v_t_lin.pdf','pdf');


%%%%%%%%%%%%%%%%%%%%%%
%
% Frequency plot
yVarName = 'freqA';
plotVBSVec;
setFigureParameters(paperSize,'time (min)','Collision frequency per A/V (1/min-m)');
saveas(gcf,'figs/freq_v_t.pdf','pdf');
%{
% original from Neil. New one separates by C* bin
figure
pl = plot(t_prod,freqA,'g-');
set(pl,'Color',[0,1,0]);
set(pl,'LineWidth',1.5);
hold
setFigureParameters(paperSize,'time (min)','Collision frequency per A/V (1/min-m)');
saveas(gcf,'figs/freq_v_t.pdf','pdf');
%}

xVarName = 't_prod';
xLabelStr = 'time (min)';

% Plot the HOM concentration in number concentration (only low C*)
plotCmd = 'plot';
yVarName = 'Nv';
fileSuffix = 'lin_t';
plotVBSVec;
pl = eval(['plot(',xVarName,'(plotSel),Nv_tot(plotSel),''k--'')']);
set(pl,'LineWidth',1.5);
setFigureParameters(paperSize,xLabelStr,'HOM (cm^{-3})');
saveas(gcf,['./figs/Nv_v_',fileSuffix,'.pdf'],'pdf');

fileSuffix = 't';
plotCmd = 'semilogy';
diagnosticPlots;

pause;
%close all;

% Plot the diagnostics again, this time vs. Dp

xVarName = 'Dp_prod';
xLabelStr = 'Diameter (nm)';
fileSuffix = 'Dp';
plotCmd = 'semilogy';
diagnosticPlots;

% Plot condensation flux
yVarName = 'C_flux';
plotCmd = 'semilogx';

plotVBSVec;
pl = eval([plotCmd,'(',xVarName,'(plotSel),C_flux_tot(plotSel),''k--'')']);
set(pl,'LineWidth',1.5);
xlim([1 30]);
ylim([0 100]);
setFigureParameters(paperSize,xLabelStr,'Flux per area ');
saveas(gcf,['./figs/flux_v_',fileSuffix,'_logx.pdf'],'pdf');

% Plot growth rate
yVarName = 'GR';
plotCmd = 'semilogx';

plotVBSVec;
pl = eval([plotCmd,'(',xVarName,'(plotSel),GR_tot(plotSel),''k--'')']);
set(pl,'LineWidth',1.5);
pl = eval([plotCmd,'(',xVarName,'(plotSel),GR_tot_max(plotSel),''r-.'')']);
set(pl,'LineWidth',1.5);
xlim([1 parms.Dpmax]);
ylim([0 parms.GRmax]);
setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
saveas(gcf,['./figs/GR_v_',fileSuffix,'_logx.pdf'],'pdf');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot growth rate
    % here assigns log C* = -8 to -5 as ELVOC
    xVarName = 'Dp_prod';
    yVarName = 'GR';
    fileSuffix = 'Dp_stacked';

    figure
    pl = area(Dp_prod, GR);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    
    for v = 9
        set(pl(v), 'FaceColor', [0.9 1 0.9]);
    end
    %{
    pl = area(Dp_prod, GR_tot);
    set(pl,'LineWidth',1.5);
    
    plotCmd = 'plot';
    pl = eval([plotCmd,'(',xVarName,'(plotSel),GR_tot(plotSel),''k--'')']);
    set(pl,'LineWidth',1.5);
    pl = eval([plotCmd,'(',xVarName,'(plotSel),GR_tot_max(plotSel),''r-.'')']);
    set(pl,'LineWidth',1.5);
    %}
    xlim([1.3 parms.Dpmax]);%*.58]);
    ylim([0 parms.GRmax]);%*2.1]);
    setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
    set(gca,'XScale','log');
    set(gca,'layer','top');
    saveas(gcf,['./figs/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf');
    
    % Plot growth rate
    % here assigns log C* = -8 to -4 as ELVOC    
    
    figure
    pl = area(Dp_prod, GR);
    for v = (1:5)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/9);
    end
    for v = (6:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = 9
        set(pl(v), 'FaceColor', [0.9 1 0.9]);
    end    
    xlim([1.3 parms.Dpmax]);
    ylim([0 parms.GRmax]);
    setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
    set(gca,'XScale','log');
    set(gca,'layer','top');
    saveas(gcf,['./figs/GR_v_',fileSuffix,'_logx_color2.pdf'],'pdf');

pause;
% close all;

return
%{
%%
% Growth Rate vs Diameter
growthRate = phiArray1 .* (2./(parms.rhoOrg*parms.Np*pi*Dp_dil.^2) ) * 6e13; % in nm/h

figure
pl = semilogx(Dp_dil,growthRate, 'g-');
set(pl,'Color',[0,1,0]);
set(pl,'LineWidth',1.5);
ylim([0 30]);

setFigureParameters(paperSize,'Particle Diameter (nm)','Growth Rate (nm/h)');
saveas(gcf, 'figs/testC_GrowthRate.pdf','pdf');

% Growth Rate vs Diameter
figure
growthRate = [0; diff(Dp_dil)/(.05/60)]; % in nm/h
p  = 1e-2;           % initialize smoothing constant
fn = csaps(Dp_dil, growthRate, p); % get ppform of the cubic smoothing spline
y1 = ppval(fn, Dp_dil);   % evaluate piecewise polynomial

pl = semilogx(Dp_dil,y1, 'g-');
set(pl,'Color',[0,1,0]);
set(pl,'LineWidth',1.5);
setFigureParameters(paperSize,'Particle Diameter (nm)','Growth Rate (nm/h)');
ylim([0 max(growthRate)*1.1]);
saveas(gcf, 'figs/testC_GrowthRate2_noShift.pdf','pdf');
%{
figure
pl = semilogx(Dp_prod,growthRate, 'g-');
set(pl,'Color',[0,1,0]);
set(pl,'LineWidth',1.5);

setFigureParameters(paperSize,'Particle Diameter (nm)','Growth Rate (nm/h)');
saveas(gcf, 'figs/testC_GrowthRate2.pdf','pdf');
%}
%}

% Mass vs time
figure
pl = plot(t_prod,COA_prod,'g-');
set(pl,'Color',[0,1/10,0]);
set(pl,'LineWidth',1.5);
setFigureParameters(paperSize,'time (min)','suspended mass (\mu g m^{-3})');
ylim([0,max(COA_prod)]);
xlim([0,max(t_prod)*1.1]);
a1 = gca;
lims = axis(a1);
a2 = axes;
axis(lims);
setFigureParameters(paperSize,'','');
set(a2,'Color','none');
set(a2,'YAxisLocation','right');

saveas(gcf,'figs/mass_v_time.pdf','pdf');
%}