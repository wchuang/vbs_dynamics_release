
% total stack Plot

figure

if parms.timeStart < 0 
    pl = area(t_init,[Cs_init Cv_init]);

    for v = (1:9)
        set(pl(v),'FaceColor',[0 v/10 0]); 
    end

    for v = (1:9)
        set(pl(9+v),'FaceColor',[1 1 1].*v/10); 
    end

    hold
end

pl = area(t_prod,[Cs_prod Cv_prod]);

for v = (1:9)
    set(pl(v),'FaceColor',[0 v/10 0]); 
end

for v = (1:9)
    set(pl(9+v),'FaceColor',[1 1 1].*v/10); 
end


setFigureParameters(paperSize,'time (min)','C (\mu g m^{-3})');
xlim([min(tProd),max(tDil)]);
ylim([0,1].*parms.maxY*2);
a1 = gca;
lims = axis(a1);

a2 = axes;
axis(lims);
setFigureParameters(paperSize,'','');

set(a2,'Color','none');
set(a2,'YAxisLocation','right');
%ylim([0,1].*parms.maxY*2/parms.dilRatio);


saveas(gcf,'./figs/Ctot_lin.pdf','pdf');




