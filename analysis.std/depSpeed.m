function [ vdep, vGR ] = depSpeed(Dp, Morg, rhoOrg, Di )

% depSpeed calculate the deposition speed, also gives growth rate speed
%   Dp is particle diameter                     nm
%   Morg is molar weight defaults to parms      amu
%   rhoOrg is density, defaults                 kg/m3


global parms

physicalConstants;

if nargin < 3
    rhoOrg = parms.rhoOrg;
end
if nargin < 4
    Di = parms.Dorg;
end

% Figure out the particle mass in amu from the diameter in nm
Vp = pi/6 * (Dp*nm2m).^3;
Mp = Vp * rhoOrg / amutokg;

% Figure out the reduced mass of the collision (vapor and particle)
if nargin < 2
    Morg = parms.Morg;
end 

% The reduced mass includes (monodisperse) particle mass
mu = bsxfun(@times, Morg', Mp) ./ bsxfun(@plus, Morg', Mp); % size(length(t), length(C*))

% Obtain the center of mass speed of the collision
speed = meanSpeed(mu,287)/4; % size(length(t), length(C*))

% Molecular enhancement (convert cross section to surface
e_ip = (Dp+Di).^2 ./ Dp.^2;

% Calc cond sink, comes up in 1/s so convert to 1/min
beta = FuchsSutugun(Dp,parms.lambda,parms.alphaOrg); % size(length(t), 1)

vdep = parms.alphaOrg .* e_ip .* speed .* beta;

vGR = 3600 * 2 * vdep / parms.rhoOrg;


end

