function [ dCdt] = dynVBS( t, Ccomb )
% dynVBS has the ODEs that calculate the concentrations of produced organics in each
% reservoir, in addition to the concentrations of seeds, particles, and
% precursors.

global parms

physicalConstants;
numBins  = length(parms.y);
numSizes = length(parms.DpSeed);

% Ccomb is the combined C column vector
% This is a 9-bin VBS with whatever C*s we want
Cv = Ccomb(1:numBins);    % vapor organic concentration
Cs = Ccomb(numBins +(1:numBins*numSizes));  % suspended (particle) organic concentration
Ct = Ccomb(numBins*(numSizes+1) +(1:numBins)); % vapor organics on walls
Cd = Ccomb(numBins*(numSizes+2) +(1:numBins*numSizes)); % susp organics in particle form on walls
Cseed    = Ccomb(numBins*(2*numSizes+2)+(1:length(parms.DpSeed)));  % seed mass concentration
Cseedw   = Ccomb(numBins*(2*numSizes+2)+  length(parms.DpSeed)+(1:length(parms.DpSeed)));
Np       = Ccomb(numBins*(2*numSizes+2)+2*length(parms.DpSeed)+(1:length(parms.Np)));     % number concentration
Npw      = Ccomb(numBins*(2*numSizes+2)+2*length(parms.DpSeed)+  length(parms.Np)+(1:length(parms.Np)));
Cprec    = Ccomb(numBins*(2*numSizes+2)+2*length(parms.DpSeed)+2*length(parms.Np)+1);  % precursor concentration
CprecTot = Ccomb(numBins*(2*numSizes+2)+2*length(parms.DpSeed)+2*length(parms.Np)+2); % reacted precursor concentration

rate = parms.k_O3AP * (Cprec) * (parms.C_O3)*60; % in ug/m3/min; no factor of 1.8 because of OH scavenger

% calculate total suspended OA and then activities (as)
COA = sum(reshape(Cs,[],numSizes),1)';
Cpart = COA + Cseed*parms.interactSeed;
Cpartrep = reshape(repmat(Cpart',[numBins 1]), [numBins*numSizes,1]); % formula to expand each diameter
if Cpart > 0, as = Cs ./ Cpartrep; else as = 0; end

%{
COAd = sum(Cd);
Cpartd = COAd + Cseedw*parms.interactSeed;
if Cpartd > 0, as_d = Cd / Cpartd; else as = 0; end
%}
% calc production flux in vapor in ug/m3-min
Pv = parms.y * (rate + parms.precTimeCoeff * t); %

% now calculate net condensation flux
% w=0: particles do not interact with vapors after they are lost to the walls
% w=1: vapors continue to interact and equilibrate with particles
% (Weitcamp et al., 2007)
% We are currently assuming that there is no concentration gradient in the
% boundary layer. This holds if diffusion is fast compared to the
% uptake by walls/particles.
% for w=0, use Np; for w=1, use parms.Np

%% vapor-particle interaction
% This may get over-written in the vapor-'deposited particle' interaction
% section
[kc, Dp, freq] = condSinkSimple(Np,COA,Cseed);
%
% need to make matrix sizes match
Dprep = reshape(repmat(Dp',[numBins 1]), [numBins*numSizes,1]);
Corep = repmat(parms.Co, [numSizes 1]); % formula to expand each C* bin
Cvrep = repmat(Cv, [numSizes 1]);
kcrep = reshape(kc', [numBins*numSizes,1]);% reshape a numSize by numBins grid to an array
%
phi_vsrep  = kcrep .* (Cvrep - as .* Corep .* 10.^(parms.DK./Dprep));

%% vapor-wall interaction
switch parms.vaporWallLossType
    case 'constant'
        k_vt = parms.k_vt; %% vapor wall loss. Can be a vector or a constant
        phi_vt = k_vt .* Cv; % rate to the walls
    case 'diffusion-limited to walls'
        [k_vtDep k_vtEvap depNumber] = wallSinkSimple();
        phi_vt =  - k_vtDep .* Cv + k_vtEvap .* Ct; % vapor to wall
    otherwise
        error('Var:vaporWallLossType', 'Enter valid type for vapor wall loss');
end

%% vapor-'deposited particle' interaction
switch parms.vaporDepositedInteraction
    case 'omega=1' % deposited particles interact as if they were in bulk
        [kc, Dp, freq] = condSinkSimple(Np+Npw,COA+COAd,Cseed+Cseedw);
        phi_vsd  = kc' .* (Cv - as .* parms.Co .* 10.^(parms.DK/Dp));      
        phi_vd = Npw/(Np+Npw) * phi_vsd;
        phi_vs = Np /(Np+Npw) * phi_vsd;
    case 'omega=0'
        phi_vd = 0;
    case 'diffusion-limited to walls'
        if Npw > 1
            [k_vtDep k_vtEvap depNumber] = wallSinkSimple();
            [k_vd, Dpw] = condSinkSimpleWall(Npw, sum(Cd), Cseedw);
            Co = (1 ./ (depNumber+1)) .* Cv + (depNumber ./ (depNumber+1)) .* Ct; % Concentration at wall
            phi_vd = k_vd' .* (Co - as_d.* parms.Co .* 10.^(parms.DK/Dpw));
        else 
            phi_vd = zeros(size(phi_vs)); %
        end
    otherwise
        error('Var:vaporDepositedInteraction', 'Enter valid type for vapor wall loss');
end
        
switch parms.particleWallLossType % particle loss
    case 'constant'
        k_sd = parms.k_pw; % [min-1]
    case 'size-dependent'
        k_sd = (parms.partWallLossA * exp(-Dp/parms.partWallLossB) + parms.partWallLossC)/60; % [min-1]
    otherwise
        error('Var:particleWallLossType', 'Please enter valid type for particle wall loss')
end
% Change in particle number
switch parms.numTimeDependence
    case 'none'
        NucRate = 0;
        CseedRate = 0;
    case 'time-dependent'
        if t < parms.timeSwitch
            % Burst stage, d/dx(f(x)) = a*b*exp(b*x)
            NucRate = parms.initA * parms.initB * exp(parms.initB * t);
            CseedRate = 1/6*pi*(parms.DpSeed*nm2m)^3 * (NucRate*cm3tom3) * parms.rhoSeed * kgm3tougm3;
        elseif t >= parms.timeSwitch
            % Leveling off stage, d/dx(f(x)) = -a*b*exp(b/x)/x^2
            NucRate = -parms.levelA * parms.levelB * exp(parms.levelB / t) / t^2;
            CseedRate = 1/6*pi*(parms.DpSeed*nm2m)^3 * (NucRate*cm3tom3) * parms.rhoSeed * kgm3tougm3;
        end
    otherwise
        error('Var:numLossType', 'Please enter valid type for number wall loss')
end

%reshaping back
phi_vs = sum(reshape(phi_vsrep, [], numSizes),2); %reshaping to C* length

%% oligomerization function - added 2016/09/05
% we draw parameters from Trump and Donahue 2014, ACP. 
switch parms.oligomerization
    case 1
        k_reverse = .4; % at 300K, [hr-1]
        Keq = 1000; % assumed, need Keq > 1 to allow for formation of dimers [unitless]
        k_forward = 20000; %Keq * k_reverse / 60; % defined as T independent (see citation page 3697 for explanation) [min-1]
        % for logC*=0 monomers, monomer-monomer dimerization cannot occur (in the particle phase)--
            % there is simply too little concentration to allow that. 
            % instead we look at dimerization of logC*=0 monomers with (E)LVOCs
            % if that is the case, then the reaction is P_olig = k_f * massFraction(HOMs) * massFraction(monomer) * COA
            % meaning that oligomerization occurs faster with more mass.
            % if SVOC is small, then massFraction(HOMs)=1, so the equation comes down to P_olig = k_f * massFraction(monomer)*COA
        dimerCreateRate = k_forward * Cs(end); % only for monodisperse
        dimerRateArray = [dimerCreateRate;0;0;0;0;0;0;0; -dimerCreateRate]; % Currently will only work for monodisperse
    case 0
        dimerRateArray = 0;
end

%% Time units are in minutes!
dCvdt = Pv - phi_vs - parms.kflow * Cv - phi_vd - phi_vt;
dCsdt = phi_vsrep - parms.kflow * Cs - (k_sd .* Cs) + dimerRateArray;
% Add in the wall area dynamics
dCtdt = phi_vt; % 't' for 'teflon'. vapor conc in/on the walls
dCddt = phi_vd + k_sd .* Cs; % 'd' for 'deposited'. assume that vapors interact between bulk vapor and
% particles on wall, but at a lower concentration.

dCseeddt = -k_sd * Cseed + CseedRate;
dCseedwdt = k_sd * Cseed;
dNpwdt = k_sd * Np; % assume irreversible
dNpdt = NucRate -k_sd * Np;
dCprecdt = -rate;
dCprecTotdt = +rate;

if t > 10
    pause
end

% repack the derivative
dCdt = [dCvdt; dCsdt; dCtdt; dCddt; dCseeddt; dCseedwdt; dNpdt; dNpwdt; dCprecdt; dCprecTotdt];

end

