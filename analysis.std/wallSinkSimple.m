function [k_vwDep k_vwEvap depNumber] = wallSinkSimple()
% Calculates the deposition rate of vapor to the wall, as a function of
% bulk vapor and vapor concentration in the Teflon wall.

% speed in [m/s] is input as a parameter

global parms
physicalConstants;

speed = meanSpeed(parms.Morg)/4; % make it a single column vector

%% Based on Zhang et al., 2015, ACP
alphaCrit = 8 .* (parms.molecDiff .* parms.coeffEddyDiff)^(1/2) ./ (pi .* speed);
depNumber = parms.alphaWall ./ alphaCrit; % dimensionless,
% is a measure of how fast diffusion is relative to deposition

wallHit = parms.alphaWall .* speed / 4; %[m/s]
%vapWallPartition = R * parms.T ./ (parms.Co .* parms.actCoeffWall .* Mw); % Zhang et al. 2015 p4199
vapWallPartition = parms.Morg ./ (parms.Mw .* parms.Co); % [m3/ug] K_{w,i} Zhang et al. 2015 p4199 
chamberSA = 4*pi*(3*parms.chamberV/(4*pi))^(2/3); % chamber surface area, assuming spherical

k_vwDep = chamberSA/parms.chamberV * wallHit ./(depNumber+1) * sec2min; % [min-1] vapor deposition to wall
k_vwEvap = k_vwDep ./(vapWallPartition .* parms.massOfWall); % [min-1] vapor evaporation from wall

end