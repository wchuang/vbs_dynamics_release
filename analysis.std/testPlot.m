verbos = num2str(parms.plotVerbosity);
warning('off','MATLAB:Figure:SetPosition') % this is temporary, just to avoid warnings. The warning is turned on at the bottom of the script
if exist(['./figs' verbos], 'dir') ~= 7
    mkdir(['./figs' verbos])
end

calcForPlots;

%% stacked plot of where all the organics are
    xVar = 't_prod';
    xLabelStr = 'time (min)';
    fileSuffix = 't_stacked';
    plotCmd = 'area';

    figure
    C = [COA_prod Cd_prodSum Ct_prodSum Cvap_prod C_prec*sum(parms.y)];
    pl = area(t_prod, C);
    
    set(pl(1), 'FaceColor', 'green' );    
    set(pl(2), 'FaceColor', 'blue'  );
    set(pl(3), 'FaceColor', 'red'  );
    set(pl(4), 'FaceColor', [.5 .5 .5]);
    set(pl(5), 'FaceColor', 'cyan');
    
    setFigureParameters(paperSize,xLabelStr,'Organic Mass (\mu g m^{-3})');
    set(gca,'layer','top')
    yl = ylim;
    legend(fliplr(pl),{'Precursor Yield Potential','Bulk vapor','Vapors on Teflon','Particles on Walls','Bulk suspended'}, 'Location', 'SouthEast')
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix],'fig');
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix,'.pdf'],'pdf'); 
    % add in an inset zooming in on the first few minutes
    %set(handaxes1, 'Box', 'off')
    handaxes3 = axes('Position', [0.20 0.55 0.3 0.3]);
    pl = area(t_prod, C);    
    set(pl(1), 'FaceColor', 'green' );    
    set(pl(2), 'FaceColor', 'blue'  );
    set(pl(3), 'FaceColor', 'red'  );
    set(pl(4), 'FaceColor', [.5 .5 .5]);
    set(pl(5), 'FaceColor', 'cyan');    
    %setFigureParameters(paperSize,xLabelStr,'Organic Mass (\mu g m^{-3})');
    set(gca,'layer','top')
    set(handaxes3, 'Box','on')
    xlim([0 10]);
    ylim(yl/10);
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix,'_Inset'],'fig');
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix,'_Inset','.pdf'],'pdf'); 
    %
    xVar = 'Dp_prod';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'dp_stacked';
    plotCmd = 'area';

    figure
    C = [COA_prod Cd_prodSum Ct_prodSum Cvap_prod C_prec*sum(parms.y)];
    pl = area(Dp_prod, C);
    
    set(pl(1), 'FaceColor', 'green' );    
    set(pl(2), 'FaceColor', 'blue'  );
    set(pl(3), 'FaceColor', 'red'  );
    set(pl(4), 'FaceColor', [.5 .5 .5]);
    set(pl(5), 'FaceColor', 'cyan');
    
    setFigureParameters(paperSize,xLabelStr,' Organic Mass (\mu g m^{-3})');
    set(gca,'layer','top')
    set(gca,'XScale','log');
    legend(fliplr(pl),{'Precursor Yield Potential','Bulk vapor','Vapors on Teflon','Particles on Walls','Bulk suspended','Seeds on Walls','Bulk Seeds'}, 'Location', 'NorthWest')
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix],'fig');
    saveas(gcf,['./figs/',verbos,'/C_v_',fileSuffix,'.pdf'],'pdf');     
    
%% vapor condensation sink to particles
    figure
    pl = plot(t_prod, kc_prod(:,1:8), 'g-');
    for v = (1:4)
        set(pl(v), 'Color', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'Color', [1 .9*v/8 .9]);
    end
    %{
    for v = (9:size(Cs_prodbins,2))
        set(pl(v), 'Color', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
    %}
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'time (min)','Average condensation sink (min^{-1})');
    saveas(gcf,['./figs/',verbos,'/condSink_v_t' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSink_v_t' '.pdf'],'pdf');

    figure
    pl = plot(Dp_prod, kc_prod(:,1:8), 'g-');
    for v = (1:4)
        set(pl(v), 'Color', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'Color', [1 .9*v/8 .9]);
    end
    %{
    for v = (9:size(Cs_prodbins,2))
        set(pl(v), 'Color', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
    %}
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'Diameter (nm)','Average condensation sink (min^{-1})');
    set(gca,'XScale','log');
    saveas(gcf,['./figs/',verbos,'/condSink_v_dp' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSink_v_dp' '.pdf'],'pdf');
%% ratio of vapor condensation sink to particles: vapor condensation sink to wall
    kc_ratio = kc_prod ./ parms.k_vt(1);
    figure
    pl = plot(t_prod, kc_ratio(:,1:8), 'g-');
    for v = (1:4)
        set(pl(v), 'Color', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'Color', [1 .9*v/8 .9]);
    end
    %set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'time (min)','CS_{particle}/CS_{wall}');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t' '.pdf'],'pdf');
    
    figure
    pl = plot(Dp_prod, kc_ratio(:,1:8), 'g-');
    for v = (1:4)
        set(pl(v), 'Color', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'Color', [1 .9*v/8 .9]);
    end
    %set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'Diameter (nm)','CS_{particle}/CS_{wall}');
    set(gca,'XScale','log');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_Dp' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_Dp' '.pdf'],'pdf');
%% Diameter vs. time
    figure
    pl = semilogy(t_prod,Dp_prod,'g-');
    set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',5);
    setFigureParameters(paperSize,'time (min)','Particle Diameter (nm)');
    saveas(gcf,['./figs/',verbos,'/diameter_v_t_log_data.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/diameter_v_t_log_data.pdf'],'pdf');
    set(gca,'YScale','lin');
    saveas(gcf,['./figs/',verbos,'/diameter_v_t_lin.fig'],'fig');   
    saveas(gcf,['./figs/',verbos,'/diameter_v_t_lin.pdf'],'pdf');    
%% Growth rate - Contribution of each volatility bin
    fileSuffix = 't_stacked';
    figure
    pl = area(t_prod, GR);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = (9:size(GR,2))
        set(pl(v), 'FaceColor', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
    %xlim([1 150]);
    %ylim([0 400]);
    setFigureParameters(paperSize,'time (min)','Growth Rate (nm h^{-1})');
    set(gca,'layer','top')
    saveas(gcf,['./figs/',verbos,'/GR_v_',fileSuffix,'_logx_color'],'fig');
    saveas(gcf,['./figs/',verbos,'/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf'); 
    %
    fileSuffix = 'dp_stacked';
    figure
    pl = area(Dp_prod, GR);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = (9:size(GR,2))
        set(pl(v), 'FaceColor', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
    %xlim([1 150]);
    %ylim([0 400]);
    setFigureParameters(paperSize,'Diameter (nm)','Growth Rate (nm h^{-1})');
    set(gca,'XScale','log');
    set(gca,'layer','top')
    saveas(gcf,['./figs/',verbos,'/GR_v_',fileSuffix,'_logx_color'],'fig');
    saveas(gcf,['./figs/',verbos,'/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf');     
%% AMF vs Mass Loading
    figure        
    
    readAndPlotLowNOxForBasis;
    %{
        [data.COA data.dCROG] = readSOAAPin('2005_06_28.trunc');
        data.AMF = data.COA./data.dCROG;
        p7 = semilogx(data.COA,data.AMF,'ko');
        set(p7,'MarkerSize',12);
        set(p7,'MarkerFaceColor',[.4 .4 .4]);
    %}
    hold on
       
    % Plot the equilibrium lines
    
    fid = fopen(['./data/' 'COA_ss.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    COA_eq = C{1};
    fclose(fid);
    fid = fopen(['./data/' 'Y_ss.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    Y_eq = C{1};
    fclose(fid);    
    pl2 = semilogx(COA_eq,Y_eq,'g--'); %
    set(pl2,'LineWidth',4);
    %set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    %set(pl2,'MarkerEdgeColor','k');
    %set(pl2,'MarkerSize',10);
    uistack(pl,'top');
    
    %{
    Ctot_prod = Cs_prod + Cv_prod + Cd_prod + Ct_prod;
    [xis,COA_ss] = partitionAerosol(Ctot_prod,parms.Co);
    Y_ss = COA_ss ./ (C_precReacted+eps);    
    % Plot the steady-state yield values
    pl = plot(COA_ss,Y_ss,'g--');
    set(pl,'Color',[0,.4,0]);
    set(pl,'LineWidth',4);
    %}
    set(gca,'XScale','log');
    xlim([1e-3 1e3]);
    setFigureParameters(paperSize,'COA (\mug m^{-3})','SOA Yield');
    saveas(gcf,['./figs/',verbos,'/AMF_noDynamic.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/AMF_noDynamic.pdf'],'pdf');  
    % where readAndPlot... would go
    %}
    hold on
    
    % Make the dynamic model yields
    massYield = (COA_prod+Cd_prodSum) ./ (C_precReacted+eps);    
    xVarName = 'COA_prod+Cd_prodSum'; % (COA_prod)
    yVarName = 'massYield';
    pl = eval(['semilogx(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',4);
    
    set(gca,'XScale','log');
    %xlim([1e-6 1e1]); to see the equilibrium yields increasing through LVOC bins
    xlim([1e-3 1e3]);
    setFigureParameters(paperSize,'COA (\mug m^{-3})','SOA Yield');
    saveas(gcf,['./figs/',verbos,'/AMF.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/AMF.pdf'],'pdf');    
%% AMF v t    
    figure        
    
    % Make the dynamic model yields
    massYield = (COA_prod+Cd_prodSum) ./ (C_precReacted+eps);    
    xVarName = 't_prod'; % (COA_prod)
    yVarName = 'massYield';
    pl = eval(['plot(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',4);
    
    hold on 
       
    %xlim([1e-3 1e3]);
    setFigureParameters(paperSize,'time (min)','SOA Yield');
    saveas(gcf,['./figs/',verbos,'/AMF-t.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/AMF-t.pdf'],'pdf');
    
%% stacked total mass vs time    
%{
    xVar = 't_prod';
    xLabelStr = 'time (min)';
    fileSuffix = 't_stacked';
    plotCmd = 'area';

    figure
    C = [Cseed_prod COA_prod];
    pl = area(t_prod, C);
    
    set(pl(1), 'FaceColor', 'red');    
    set(pl(2), 'FaceColor', 'green'  );
    
        hold on
    % Plot the observations
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Time.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    t_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Mass.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    C_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Seed Mass.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    seed_obs = C{1};
    fclose(fid);
    %
        pl2 = plot(t_obs,C_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);
        pl2 = plot(t_obs,seed_obs,'.'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.2 .2 .2]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);
    
    setFigureParameters(paperSize,xLabelStr,'Mass Concentration (\mu g m^{-3})');
    set(gca,'layer','top')
    legend(fliplr(pl),{'Bulk suspended organics','Bulk suspended seed'}, 'Location', 'SouthEast')
    saveas(gcf,['./figs/',verbos,'/Mass_v_',fileSuffix],'fig');
    saveas(gcf,['./figs/',verbos,'/Mass_v_',fileSuffix,'.pdf'],'pdf'); 
%}
%% Organic:sulfate ratio
if parms.DpSeed > 60
    sulfate = Cseed_prod / 132 * 96;    
    figure
    pl = plot(t_prod, COA_prod./sulfate, 'g-');
    set(pl,'LineWidth',4);
    setFigureParameters(paperSize,'time (min)','Organic Mass:Sulfate Mass');
    saveas(gcf,['./figs/',verbos,'/OrgSulfRatio.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/OrgSulfRatio.pdf'],'pdf');
end

    %% diagnostic plots, vs time
    xVarName = 't_prod';
    xLabelStr = 'time (min)';
    fileSuffix = 't';
    plotCmd = 'semilogy';
    diagnosticPlots;    
    %% diagnostic plots, vs Dp
    xVarName = 'Dp_prod';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'Dp';
    plotCmd = 'semilogy';
    diagnosticPlots;

    
    %% plot organic and seed mass stacked
    fileSuffix = 't_stacked';
hold off
% This reads data from the polydisperse model, so that the seed mass will
% match the data better, as opposed to using the condensation-sink-weighted
% seed diameter.
if exist(['./data/' parms.expDate '/' parms.expDate ' Seed Mass Modeled.txt']) == 2
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Mass Modeled.txt']);
    C = textscan(fid,'%f','HeaderLines',0);
    Cseed_prod = C{1};
    fclose(fid);
end
figure
C = [Cseed_prod COA_prod];
pl = area(t_prod, C);
    set(pl(1), 'FaceColor', 'red'); % orange = [1 0.5 0] );    
    set(pl(2), 'FaceColor', 'green'  );
legend(fliplr(pl),{'Bulk suspended organics','Bulk suspended seed'}, 'Location', 'NorthEast')
setFigureParameters(paperSize,'time (min)','Mass Concentration (\mu g m^{-3})');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,''],'fig');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,'.pdf'],'pdf'); 
%{
    for v = (1:4)
        set(pl(v+1), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v+1), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = (9:size(Cs_prodbins,2))
        set(pl(v+1), 'FaceColor', [0 100/256 40/256]+(v-8)*[50 30 40]/256);
    end
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,'_color'],'fig');
saveas(gcf,['./figs/',verbos,'/Cs_seeds_v_',fileSuffix,'_color.pdf'],'pdf');
%}
if exist(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Seed Mass.txt']) == 2
    hold on
    % Read the observations
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Time.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    t_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Mass.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    C_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Seed Mass.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    seed_obs = C{1};
    fclose(fid);
    % Plot the observations
        pl2 = plot(t_obs,C_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);
        pl2 = plot(t_obs,seed_obs,'.'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.2 .2 .2]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);
    set(gca,'layer','top')
    saveas(gcf,['./figs/',verbos,'/Cs_seeds_data_v_',fileSuffix,'_color'],'fig');
    saveas(gcf,['./figs/',verbos,'/Cs_seeds_data_v_',fileSuffix,'_color.pdf'],'pdf');
end

%% plot condensation sink with only a single Morg.
    % Here I am using the Morg in the logC* = -4 bin
%% vapor condensation sink to particles % for a single Morg
    figure
    pl = plot(t_prod, kc_prod(:,5), 'g-');
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'time (min)','Average condensation sink (min^{-1})');
    saveas(gcf,['./figs/',verbos,'/condSink_v_t_-4Morg' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSink_v_t_-4Morg' '.pdf'],'pdf');

    figure
    pl = plot(Dp_prod, kc_prod(:,5), 'g-');
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'Diameter (nm)','Average condensation sink (min^{-1})');
    set(gca,'XScale','log');
    saveas(gcf,['./figs/',verbos,'/condSink_v_dp_-4Morg' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSink_v_dp_-4Morg' '.pdf'],'pdf');
%% ratio of vapor condensation sink to particles: vapor condensation sink to wall
    kc_ratio = kc_prod ./ parms.k_vt(1);
    figure
    pl = plot(t_prod, kc_ratio(:,5), 'g-');
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'time (min)','CS_{particle}/CS_{wall}');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t_-4Morg' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_t_-4Morg' '.pdf'],'pdf');
    
    figure
    pl = plot(Dp_prod, kc_ratio(:,5), 'g-');
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'Diameter (nm)','CS_{particle}/CS_{wall}');
    set(gca,'XScale','log');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_Dp_-4Morg' '.fig'],'fig');
    saveas(gcf,['./figs/',verbos,'/condSinkRatio_v_Dp_-4Morg' '.pdf'],'pdf');
    pause