function Kn = knudsenNumber(Dp,Mw,T,Diff);

% knudsenNumber
% be careful about units -- this is all mks

wallProperties; %contains Di in m^2/s;

if nargin < 4, Diff = Di; end;
if nargin < 3, T = 300; end;
if nargin < 2, Mw = 200; end;

Kn = (4 * Diff / meanSpeed(Mw,T)) ./ Dp;