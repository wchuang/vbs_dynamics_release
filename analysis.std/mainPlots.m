verbos = num2str(parms.plotVerbosity);
warning('off','MATLAB:Figure:SetPosition') % this is temporary, just to avoid warnings. The warning is turned on at the bottom of the script

if exist(['./figs' verbos], 'dir') ~= 7
    mkdir(['./figs' verbos])
end

calcForPlots;

if parms.plotVerbosity >= 1
  
    xVarName = 't_prod';
    xLabelStr = 'time (min)';

    %% Plot the HOM concentration in number concentration (only low C*) vs time

    plotCmd = 'plot';
    yVarName = 'Nv';
    fileSuffix = 'lin_t';
    plotVBSVec;
    pl = eval(['plot(',xVarName,'(plotSel),Nv_tot(plotSel),''k--'')']);
    set(pl,'LineWidth',1.5);
    setFigureParameters(paperSize,xLabelStr,'HOM (cm^{-3})');
    saveas(gcf,['./figs',verbos,'/Nv_v_',fileSuffix,'.pdf'],'pdf');

    %% Diameter plot vs time
    
    % Plot the model

    figure
    pl = semilogy(t_prod,Dp_prod,'g-'); % +0.3 nm for mobility diameter for Jasmin's data
    set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',5);

    hold

    % Here are the observations -- better have been read! (for Jasmin's)
    %{
    pl2 = plot(parms.t_obs,parms.Dm_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);

    uistack(pl,'top');
    %}
    %figure
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Time.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    t_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Geometric Mean Diameter.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    Dm_obs = C{1};
    fclose(fid);
    
    pl2 = plot(t_obs,Dm_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);

    uistack(pl,'top');
    
    setFigureParameters(paperSize,'time (min)','Particle Diameter (nm)');
    saveas(gcf,['figs',verbos,'/diameter_v_t_log_data.pdf'],'pdf');

    set(gca,'YScale','lin');
    saveas(gcf,['figs',verbos,'/diameter_v_t_lin.pdf'],'pdf');
    
    %% volatilty separated particle mass v time
    
    if parms.timeStart < 0
    xVarName = 't_init';
    yVarName = 'Cs_init';
    plotCmd = 'semilogy';
    plotVBSVec;
    hold on
    else
        figure
    end

    % Plot the model
    xVarName = 't_prod';
    yVarName = 'Cs_prod';

    for v = (1:4)
        fStr = ['semilogy(',xVarName,',',yVarName,'(:,v)'')'];
        pl = eval(fStr);  
        set(pl, 'Color', [.9 .9 .9]*(v+4)/8);
        set(pl,'LineWidth',4);
        hold on
    end
    for v = (5:8)
        fStr = ['semilogy(',xVarName,',',yVarName,'(:,v)'')'];
        pl = eval(fStr); 
        set(pl, 'Color', [1 .9*v/8 .9]);
        set(pl,'LineWidth',4);
    end
    for v = 9
        fStr = ['semilogy(',xVarName,',',yVarName,'(:,v)'')'];
        pl = eval(fStr); 
        set(pl, 'Color', [0.9 1 0.9]);
        set(pl,'LineWidth',4);
    end

%{    
    fStr = ['semilogy(',xVarName,',',yVarName,'(:,1),''g-'')'];
    pl = eval(fStr);    
    set(pl,'LineWidth',1.5);
    hold on
    for v = (2:9)
        pl = eval(['semilogy(',xVarName,',',yVarName,'(:,v),''g-'')']);
        set(pl,'Color',[0,v/10,0]);
        set(pl,'LineWidth',4);
    end
%}    
    Cs_tot = sum(Cs_prod,2);
    yVarName = 'Cs_tot';
    pl = eval(['semilogy(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',5);

    hold on

    %figure
    % Plot the observations
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Time.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    t_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Wall Uncorrected Mass.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    C_obs = C{1};
    fclose(fid);
    
    pl2 = semilogy(t_obs,C_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);

    uistack(pl,'top');
    
    setFigureParameters(paperSize,'time (min)','C^{particle} (\mu g m^{-3})');
    ylim([.01 15]);    
    saveas(gcf,['figs',verbos,'/Cparticle_v_t.pdf'],'pdf');
    
    %% OA:seed ratio
    %{
    OAseed = COA_prod ./ ((Np_prod*1e6)* (parms.DpSeed*1e-9)^3/6*pi* (parms.rhoSeed*1e9));
    xVarName = 't_prod';
    yVarName = 'OAseed';
        figure
    pl = eval(['plot(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',4);
    setFigureParameters(paperSize,'time (min)','OA:seed Ratio');   
    saveas(gcf,['figs',verbos,'/OAseedvst.pdf'],'pdf');
    
    xVarName = 'Dp_prod';
        figure
    pl = eval(['plot(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',4);
    setFigureParameters(paperSize,'Particle Diameter (nm)','OA:seed Ratio');   
    saveas(gcf,['figs',verbos,'/OAseedvsDp.pdf'],'pdf'); 
    %}
    
    %% Aerosol Mass Yield v Mass Loading
    figure
    
    % Make the dynamic model yields
    massYield = (COA_prod+Cd_prodSum) ./ (C_precReacted+eps);    
    xVarName = 'COA_prod+Cd_prodSum'; % (COA_prod)
    yVarName = 'massYield';
    pl = eval(['semilogx(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',4);
    
    hold on 
       
    % Plot the equilibrium lines
    fid = fopen(['./data/' 'COA_ss.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    COA_eq = C{1};
    fclose(fid);
    fid = fopen(['./data/' 'Y_ss.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    Y_eq = C{1};
    fclose(fid);    
    pl2 = semilogx(COA_eq,Y_eq,'b--'); % took out -0.3 nm diameter
    set(pl2,'LineWidth',4);
    %set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    %set(pl2,'MarkerEdgeColor','k');
    %set(pl2,'MarkerSize',10);
    uistack(pl,'top');
    
    %{
    % COAll is the total organic material, so this is the instantaneous yield
    COAll_prod = COA_prod + Cvap_prod; % ug/m3   

    % Calculate the steady-state yields
    Ctot_prod = Cs_prod + Cv_prod;
    [xis,COA_ss] = partitionAerosol(Ctot_prod,parms.Co);
    Y_ss = COA_ss ./ (C_precReacted+eps);
    
    % Plot the steady-state yield values
    pl = plot(COA_ss,Y_ss,'g--');
    set(pl,'Color',[0,.4,0]);
    set(pl,'LineWidth',4);
    %}
    
    readAndPlotLowNOxForBasis;
    
    set(gca,'XScale','log');
    xlim([1e-6 1e1]);
    setFigureParameters(paperSize,'COA (\mug m^{-3})','SOA Yield');   
    saveas(gcf,['figs',verbos,'/Odum_plot_2.pdf'],'pdf');
    
    %% Growth Rate v Time, Dp
    xVarName = 'Dp_prod';
    yVarName = 'GR';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'Dp_stacked';
    plotCmd = 'area';

    figure
    pl = area(Dp_prod, GR);
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    for v = 9
        set(pl(v), 'FaceColor', [0.9 1 0.9]);
    end
    xlim([1 150]);
    ylim([0 400]);
    setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
    set(gca,'XScale','log');
    set(gca,'layer','top')
    saveas(gcf,['./figs',verbos,'/GR_v_',fileSuffix,'_logx_color'],'fig');
    saveas(gcf,['./figs',verbos,'/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf');     
    
    % total suspended mass
    %{
    figure
    pl = plot(t_prod, COA_prod, 'g-');
    set(pl,'Color',[0,1,0]);
    set(pl,'LineWidth',5);
    setFigureParameters(paperSize,'time (min)','Total COA (\mu g m^{-3})');
    saveas(gcf,['figs',verbos,'/COA_v_t.pdf'],'pdf');
    %}
%{
     [UpperPath,Folder] = fileparts(pwd);
    
    if strcmp(Folder, 'increasingHOM')
    
        %% Stacked plot, growth rate vs Dp
        % here assigns log C* = -8 to -5 as ELVOC
        xVarName = 'Dp_prod';
        yVarName = 'GR';
        xLabelStr = 'Diameter (nm)';
        fileSuffix = 'Dp_stacked';
        plotCmd = 'area';

        figure
        pl = area(Dp_prod+0.3, GR);
        for v = (1:4)
            set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
        end
        for v = (5:8)
            set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
        end
        for v = 9
            set(pl(v), 'FaceColor', [0.9 1 0.9]);
        end

        xlim([1.3 parms.Dpmax]);
        ylim([0 parms.GRmax]);
        setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
        set(gca,'XScale','log');
        set(gca,'layer','top')
        saveas(gcf,['./figs',verbos,'/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf');   
    

        dp_meas = [1.4; 2.21; 3.44; 4.55; 5.38; 7; 11.78];
        GR_meas = [1.063; 4.151; 7.862; 11.683; 14.577; 19.954; 24.697];
        index = zeros(7,1);
        GR_mod = zeros(7,1);
        for i = 1:7
            diff = abs(dp_meas(i) - (Dp_prod+0.3));
            index(i) = find(diff == min(diff));
            GR_mod(i) = GR_tot(index(i));
        end
    end
   
    if strcmp(Folder, 'constanthom')  
        
        %% Stacked plot, growth rate vs Dp
        % here assigns log C* = -8 to -5 as ELVOC
        xVarName = 'Dp_prod';
        yVarName = 'GR';
        xLabelStr = 'Diameter (nm)';
        fileSuffix = 'Dp_stacked';
        plotCmd = 'area';

        figure
        pl = area(Dp_prod+0.3, GR);
        for v = (1:4)
            set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
        end
        for v = (5:8)
            set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
        end
        for v = 9
            set(pl(v), 'FaceColor', [0.9 1 0.9]);
        end

        xlim([1.3 parms.Dpmax]);
        ylim([0 45]);
        setFigureParameters(paperSize,xLabelStr,'Growth Rate (nm h^{-1})');
        set(gca,'XScale','log');
        set(gca,'layer','top')
        saveas(gcf,['./figs',verbos,'/GR_v_',fileSuffix,'_logx_color.pdf'],'pdf');   

        
        dp_meas = [1.653; 6.764; 20.8];
        GR_meas = [3.464; 7.107; 6.5];
        index = zeros(3,1);
        GR_mod = zeros(3,1);

        for i = 1:3
            diff = abs(dp_meas(i) - (Dp_prod+0.3));
            index(i) = find(diff == min(diff));
            GR_mod(i) = GR_tot(index(i));
        end
    end
    
%}
    
    %% molecules per particle vs Dp
    %{
    mass_particle = bsxfun(@rdivide, Cs_prod, Np_prod*cm3tom3); % organic mass in each bin, per particle
    mol_particle = bsxfun(@rdivide, mass_particle, parms.Morg'*kgm3tougm3*g2kg); % moles in each bin, per particle
    mlc_particle = mol_particle * avogadroConstant; % molecules in each bin, per particle

    figure
    pl = area(Dp_prod+0.3, mlc_particle);
    
    for v = (1:4)
        set(pl(v), 'FaceColor', [.9 .9 .9]*(v+4)/8);
    end
    for v = (5:8)
        set(pl(v), 'FaceColor', [1 .9*v/8 .9]);
    end
    
    for v = 9
        set(pl(v), 'FaceColor', [0.9 1 0.9]);
    end
    
    xlim([1.3 29]);
    %ylim([0 max(mlc_particle)]);
    setFigureParameters(paperSize,xLabelStr,'Molecules per particle');
    set(gca,'XScale','log');
    set(gca,'layer','top')
    saveas(gcf,['./figs',verbos,'/Molec_v_',fileSuffix,'_logx_color_full.pdf'],'pdf');
    
    figure
    pl = area(Dp_prod+0.3, mlc_particle(:,9));
    set(pl, 'FaceColor', [0.9 1 0.9]);
    
    xlim([1.3 29]);
    %ylim([0 max(mlc_particle)]);
    setFigureParameters(paperSize,xLabelStr,'Molecules per particle');
    set(gca,'XScale','log');
    set(gca,'layer','top')
    saveas(gcf,['./figs',verbos,'/Molec_v_',fileSuffix,'_logx_color_1_full.pdf'],'pdf');
    %}
    
    %% Stack plot of mass
    xLabelStr = 'time (min)';
    fileSuffix = 't_stacked';
    plotCmd = 'area';

    figure
    C = [COA_prod Cd_prodSum Ct_prodSum Cvap_prod C_prec*sum(parms.y)];
    pl = area(t_prod, C);
    
    set(pl(1), 'FaceColor', 'green' );    
    set(pl(2), 'FaceColor', 'blue'  );
    set(pl(3), 'FaceColor', 'red'  );
    set(pl(4), 'FaceColor', [.5 .5 .5]);
    set(pl(5), 'FaceColor', 'cyan');
    
    setFigureParameters(paperSize,xLabelStr,'Mass (\mu g m^{-3})');
    set(gca,'layer','top')
    legend(fliplr(pl),{'Precursor Yield Potential','Bulk vapor','Vapors on Teflon','Particles on Walls','Bulk suspended'}, 'Location', 'SouthEast')
    saveas(gcf,['./figs',verbos,'/C_v_',fileSuffix],'fig');
    saveas(gcf,['./figs',verbos,'/C_v_',fileSuffix,'.pdf'],'pdf'); 
end

if parms.plotVerbosity >= 2
    %% Condensation sink plot vs time
    xVarName = 't_prod';
    yVarName = 'kc_prod';
    plotCmd = 'plot';
    plotVBSVec;
    setFigureParameters(paperSize,'time (min)','Average condensation sink (min^{-1})');
    saveas(gcf,['figs',verbos,'/condSink_v_t' '.pdf'],'pdf');
    %{
    xVarName = 't_prod';
    yVarName = 'kcw_prod';
    plotCmd = 'plot';
    plotVBSVec;
    setFigureParameters(paperSize,'time (min)','Average condensation sink (min^{-1})');
    saveas(gcf,['figs',verbos,'/condSinkWall_v_t' '.pdf'],'pdf');    
    %}
    %% Plot condensation flux vs time
    yVarName = 'C_flux';
    plotCmd = 'semilogx';
    plotVBSVec;
    pl = eval([plotCmd,'(',xVarName,'(plotSel),C_flux_tot(plotSel),''k--'')']);
    set(pl,'LineWidth',3);
    xlim([1 30]);
    ylim([0 100]);
    setFigureParameters(paperSize,xLabelStr,'Flux per area ');
    saveas(gcf,['./figs',verbos,'/flux_v_',fileSuffix,'_logx.pdf'],'pdf');    
    
    %% Frequency plot vs time
    xVarName = 't_prod';
    yVarName = 'freqA';
    plotVBSVec;
    setFigureParameters(paperSize,'time (min)','Collision frequency per A/V (1/min-m)');
    saveas(gcf,['./figs',verbos,'/freq_v_t.pdf'],'pdf');
    
    %% Plot Np vs time
    figure
    xVarName = 't_prod';
    yVarName = 'Np_prod';
    fileSuffix = 'lin_t';
    
    pl = eval(['plot(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',3);
    
    hold on
    
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Time.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    t_obs = C{1};
    fclose(fid);
    fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Number Concentration.txt']);
    C = textscan(fid,'%f','HeaderLines',1);
    Np_obs = C{1};
    fclose(fid);
    
    pl2 = plot(t_obs,Np_obs,'o'); % took out -0.3 nm diameter
    set(pl2,'MarkerFaceColor',[.4 .4 .4]);
    set(pl2,'MarkerEdgeColor','k');
    set(pl2,'MarkerSize',10);

    uistack(pl,'top');
    
    setFigureParameters(paperSize,'time (min)','seed concentration (cm^{-3})');   
    saveas(gcf,['figs',verbos,'/Np_v_',fileSuffix,'.pdf'],'pdf');
    
    %% Plot precursor concentration vs time
    figure
    xVarName = 't_prod';
    yVarName = 'C_prec';
    pl = eval(['plot(',xVarName,',',yVarName,'(:,1),''g-'')']);
    set(pl,'LineWidth',3);
    setFigureParameters(paperSize,'time (min)','precursor concentration (\mu g m^{-3})');   
    saveas(gcf,['figs',verbos,'/prec_v_',fileSuffix,'.pdf'],'pdf');
       
end

if parms.plotVerbosity >= 3

    %% diagnostic plots, vs time
    xVarName = 't_prod';
    xLabelStr = 'time (min)';
    fileSuffix = 't';
    plotCmd = 'semilogy';
    diagnosticPlots;
    
    %% diagnostic plots, vs Dp
    xVarName = 'Dp_prod';
    xLabelStr = 'Diameter (nm)';
    fileSuffix = 'Dp';
    plotCmd = 'semilogy';
    diagnosticPlots;
end

hold off
warning('on','MATLAB:Figure:SetPosition')