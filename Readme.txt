v1.0

Start Matlab from the dynamics_release folder. The startup.m file will call scripts to add other folders to the Matlab path.

Standard parameters are located in setupVBSstd, and specific parameters that override the standard parameters are located in setupVBS. The global parms structure contains these parameters and are called throughout the script.

The main dependency of the code is:
	RunMe
		setupVBS
			setupVBSstd
				processObservations % note: the plots produced from this file is older and the plots can be ignored. The yield distribution is set in setupVBS, and overrides the one read into processObservations.
				readDiameters
		simpleVBSDynamics
			physicalConstants
			vaporBuildup
				@dynVBSbuildup
			@dynVBS
			testPlots	 

			
Additional files:
	ReadConcFile.m - located in the setup.std folder
		This reads Jasmin's data on the molecular weight of products and calculates a mass-concentration-weighted average MW for each volatility bin in the VBS.
		Run the file after starting Matlab from a run folder.
	ReadAPO3File - located in the setup.std folder
		This reads Jasmin's data on HOM concentrations.
