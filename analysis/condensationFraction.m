function condensationFraction(cStarMult,DK,Dps)

if nargin < 3,
    Dps = (0.3:.1:30); % particle diameters
end

if nargin < 1, 
    cStarMult = 1; % scaling for uncertainty in C*
end

if nargin < 2, 
    DK = 3; % The assumed Kelvin diameter
end

% open the ELVOC mass distribution file
fid = fopen('./data2/GasDistribution.txt');
C = textscan(fid,'%f%f','HeaderLines',1);
fclose(fid);

% pull out the two vectors from the input file
Cstar = C{1};
Conc = C{2};

% turn them around
Cstar = Cstar(end:-1:1) + log10(cStarMult);
Conc = Conc(end:-1:1);
CELVOC = sum(Conc);

fHOMs = [];


for Dp = Dps

    % Figure out the effective saturation ratio
    sumSrat = cumsum(Conc)./(10.^Cstar .* 10^(DK/Dp));

    % now show condensing fraction
    fCond = 1./(1 + 1./sumSrat);

    % now show condensing fraction
    fELVOC = cumsum(Conc.* fCond) / CELVOC;

    finalF = fELVOC(end);
    
    fHOMs = [fHOMs finalF];

end

% now make a figure
figure 


pl = semilogx(Dps,fHOMs,'g-');
set(pl,'LineWidth',2);

setFigureParameters([6.5,3],'D_p (nm)','Condensing Fraction');
ylim([0,1]);
xlim([min(Dps),max(Dps)]);

saveas(gcf,'./figs/fvsD_CLOUD.pdf','pdf');
