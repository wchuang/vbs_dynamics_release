function [COA dCROG] = readSOA(fname,path,headerlines)

if nargin < 2, path = 'data/PriorYields/'; end;
if nargin < 3, headerlines = 2; end;


[COA dROG] = textread([path fname '.dat'],'%f %f','headerlines',headerlines);

%must add back in the conversion to dROG!!!  Erased by accident

M = 136.23; % g mole-1

dCROG = dROG * 1e-9 * 2.5e19; % this is how many molecules cm-3
dCROG = dCROG / 6.02e23; %this is moles cm-3
dCROG = dCROG * M * 1e12; %this is gm cm-3 

