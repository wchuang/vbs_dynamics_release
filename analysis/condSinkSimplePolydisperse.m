function [ kc, Dp, freqPerArea] = condSinkSimplePolydisperse(Np, COA, Cseed)
%condSinkSimple calculate condensation sink also returns particle diameter
%   Np is number concentration of particles     1/cm3
%   COA is organic aerosol concentration        ug/m3
%   Cseed is seed concentration                 ug/m3
%   speed is the mean molecular speed
%  size notations in the comments denote the size of the arrays after dynamic runs are finished.
%  It is called from the plotting files.

global parms

physicalConstants;

% Figure out the particle volume in m3
% Assume monodispersed and uniform concentration
Vp = (Cseed/parms.rhoSeed + COA/parms.rhoOrg)./((Np*cm3tom3) * kgm3tougm3); %size(length(t),1)

% Now assume a sphere and volume mixing of seed and OA to figure out Dp
Dp = ((6/pi)*Vp).^(1/3) / nm2m; %size(length(t),1) [nm]

% And the particle mass in amu
Mp = (Cseed + COA)./((Np*cm3tom3))* ugtoamu; % size(length(t),1)

% Figure out the reduced mass of the collision (vapor and particle)

% The reduced mass includes (monodisperse) particle mass
mu = zeros(size(Np,1),size(Np,2)*length(parms.Morg));
for i = 1:size(Np,2)
    mu(:,i*(1:length(parms.Morg))) = bsxfun(@times, parms.Morg', Mp(:,i))./ bsxfun(@plus, parms.Morg', Mp(:,i));
end    

% In this version the speed is the center of mass collision speed 
speed = meanSpeed(mu)/4; % size(length(t), length(C*)) [m/s]

% Calc cond sink, comes up in 1/s so convert to 1/min
beta = FuchsSutugun(Dp,parms.lambda,parms.alphaOrg); % size(length(t), 1)

% Molecular enhancement (convert cross section to surface)
e_ip = (Dp+parms.Dorg).^2 ./ Dp.^2;

% The units of the frequency per area are 
%   1/time * area/volume = 1/time-length
%   here it is 1/(min-m)
freqPerArea = bsxfun(@times, speed, kron(beta,ones(1,length(parms.y)))) * sec2min; % size(length(t), length(C*))
freqPerArea = bsxfun(@times, freqPerArea, kron(e_ip, ones(1,length(parms.y)))); % 

% Calculate the aerosol particle surface area
surfArea = (pi*(Dp*nm2m).^2);

% Calc cond sink, comes up in 1/s so convert to 1/min
% addition of time variable requires a transposition for the dynamic
% portion of the code ( dynVBS has kc transposed when
% calculating phic_p)
kc = bsxfun(@times, freqPerArea, kron(surfArea,ones(1,length(parms.y))) .* (kron(Np,ones(1,length(parms.y)))*cm3tom3) );

if strcmp(parms.expDate, 'test') && parms.constantCS == 1
    kc = 0.1*ones(size(COA));
end

end

