function [Cst,y] = processObservations(cStarMult,yELVOC,chargeE)
% This file reads in observations from CLOUD, applies a correction to
% the highest volatility bin, and plots the distribution over the
% volatility basis set.
% For the dynamic model, the output from this file is extraneous and can
% be ignored. The distribution is set in the setupVBS file as parms.y.


global parms
set(0,'DefaultFigureWindowStyle','docked')

verbos = num2str(parms.plotVerbosity);

if exist(['./figs/' verbos], 'dir') ~= 7
    mkdir(['./figs/' verbos])
end
if exist(['./figs/' verbos '/distribution'], 'dir') ~= 7
    mkdir(['./figs/' verbos '/distribution'])
end  

if nargin < 2, 
    yELVOC = 0.12; % The assumed ELVOC mass yield
end

if nargin < 1, 
    cStarMult = 1; % scaling for uncertainty in C*
end

if nargin < 3,
    chargeE = 1;
end


% open the ELVOC mass distribution file
fid = fopen('./data/GasDistribution.txt');
C = textscan(fid,'%f%f','HeaderLines',1);
fclose(fid);

% pull out the two vectors from the input file
Cstar = C{1};
Conc = C{2};

% turn them around
decades = log10(cStarMult);
Cstar = Cstar(end:-1:1) + decades;
Conc = Conc(end:-1:1);

% this is amazingly clunky, but if we are making the stuff less volatile
% we need to make sure we have bins up to at least 100 ug/m3
if decades < 0
    bigCstar = Cstar(end);
    for n = (decades:-1)
        Cstar = [Cstar; bigCstar - n];
        Conc = [Conc; 0];
    end
end

% now make a figure
figure 
setFigureParameters([6.5,3],'log_{10} C^* (\mu g m^{-3})','C (\mu g m^{-3})');

bar(Cstar,Conc,'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('C (\mu g m^{-3})');

saveas(gcf,['./figs/',verbos,'/distribution','/C_CLOUD.pdf'],'pdf');
%pause


% Now figure out the mass yields, assuming that all C* <= 0 are "ELVOC"
sel = Cstar <= log10(parms.CstarShift) + 1;
CELVOC = sum(Conc(sel));

AMF = yELVOC/CELVOC * Conc;

bar(Cstar,AMF,'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('mass yield');



saveas(gcf,['./figs/',verbos,'/distribution','/AMF_CLOUD.pdf'],'pdf');
%pause

% Now collect this into a VBS ending at 1-e5 but with a 1e-8 SLVOC bin
% SLVOC = super low volatility 
sel = Cstar <= -8;
YSLVOC = sum(AMF(sel));

% The rest of the VBS will be from 1e-7 to 1
sel = (Cstar >= -7) & (Cstar < 1);
AMFbase = AMF(sel);
Cstarbase = Cstar(sel);


AMFVBS = [YSLVOC; AMFbase];
CstarVBS = [-8; Cstarbase];

% Now adjust by charging efficiency
AMFVBS = AMFVBS ./ chargeE;
AMFVBS(end) = 0.10;   % yield in 1 ug/m3 bin

AMFAPiTOF = AMFVBS .* chargeE;

% I think the yields are x2 too high for some reasonbut this is a hack
br = bar(CstarVBS,[AMFAPiTOF,AMFVBS-AMFAPiTOF]/2,'stack');

set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
set(br(2),'FaceColor',[.75 1 .75]);
set(br(2),'LineWidth',2);

xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('product distribution');
set(gca,'YTickLabel',[]);
set(gca,'YTick',[]);
%ylabel('mass yields');

saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUD.pdf'],'pdf');

%% background shading
grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 5.5, .10],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel,'Linestyle','none');
uistack(ELVOC, 'bottom');

LVOC  = rectangle('Position',[-4.5, 0, 4, .10],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, .10],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, 0.105, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, 0.105, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.75, 0.105, 'SVOC','HorizontalAlignment','center','FontSize',16);

set(gca,'layer','top')

saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUD_color.pdf'],'pdf');
%{
ELVOC2 = rectangle('Position',[-10, 0, 6.5, .06],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel,'Linestyle','none');
uistack(ELVOC2, 'bottom');
uistack(ELVOC2, 'up');
uistack(ELVOC2, 'up');

delete(t2);
t2 = text(-2, 0.063, 'LVOC','HorizontalAlignment','center','FontSize',16);

set(gca,'layer','top')
saveas(gcf,'./figs/VBS_CLOUD_color2.pdf','pdf');
%}

%% empty VBS plot
br = bar(CstarVBS,[AMFVBS]*0,'stack');
xlabel('log_{10} C^* (\mu g m^{-3})');
set(gca,'YTickLabel',[]);
set(gca,'YTick',[]);

grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 5.5, .10],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');

LVOC  = rectangle('Position',[-4.5, 0, 4, .10],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, .10],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  

t1 = text((-8.5-4.5)/2, 0.105, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, 0.105, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.75, 0.105, 'SVOC','HorizontalAlignment','center','FontSize',16);

xlim([-9 2]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUD_color_blank.fig'],'fig');
saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUD_color_blank.pdf'],'pdf');

%% for finer gradation
figure
br = bar(CstarVBS,[AMFVBS]/2,'stack');
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('product distribution');
set(gca,'YTickLabel',[]);
set(gca,'YTick',[]);

grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 2.5, .10],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*5/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-7.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-6.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*7/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-5.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*8/8,'Linestyle','none');
uistack(ELVOC, 'bottom');

LVOC  = rectangle('Position',[-4.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*5/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-3.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-2.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*7/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');
LVOC  = rectangle('Position',[-1.5, 0, 1, .10],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*8/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, .10],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, 0.105, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, 0.105, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.75, 0.105, 'SVOC','HorizontalAlignment','center','FontSize',16);

xlim([-9 2]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUD_color_total.pdf'],'pdf');

%%
% set up return parameters
Cst = 10.^CstarVBS;
y = AMFVBS;


% % continue with some more stuff
% now show the cumulative concentrations
figure
bar(Cstar,cumsum(Conc),'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('\Sigma C (\mu g m^{-3})');

saveas(gcf,['./figs/',verbos,'/distribution','/sumC_CLOUD.pdf'],'pdf');
% pause

% now show the saturation ratios

% include the VBS SOA
Conc(end-0) = 0.02 * CELVOC/yELVOC;


sumSrat = cumsum(Conc)./(10.^Cstar);
bar(Cstar,log10(sumSrat),'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('log_{10} S^{eff} = (\Sigma C) / C_i^*');
ylim([-5 +5]);

saveas(gcf,['./figs/',verbos,'/distribution','/S_CLOUD.pdf'],'pdf');


% now show condensing fraction
fCond = 1./(1 + 1./sumSrat);
bar(Cstar,fCond,'g');
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('Condensing fraction');

saveas(gcf,['./figs/',verbos,'/distribution','/fCond_CLOUD.pdf'],'pdf');


% now show condensing fraction
fELVOC = cumsum(Conc.* fCond) / CELVOC;
bar(Cstar,fELVOC,'g');
ylim([0,1]);
xlabel('log_{10} C^* (\mu g m^{-3})');
ylabel('Fraction ELVOC condensed');

saveas(gcf,['./figs/',verbos,'/distribution','/fELVOC_CLOUD.pdf'],'pdf');

%% mass yield plot, be very careful in using this plot, a lot of variables are changed to produce different versions
paperSize = [9 5];

figure
        %use only for the dynamicVBS-ELVOC-paper mass yields
        sel = (parms.Co == 10^-1); AMFVBS(sel) = 0; %
        sel = (parms.Co == 10^-0); AMFVBS(sel) = 0; % 
        y = AMFVBS;
br = bar(CstarVBS,[AMFVBS],'stack');
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
setFigureParameters(paperSize,'log_{10} C^* (\mu g m^{-3})','mass yield');
%set(gca,'YTickLabel',[]);
%set(gca,'YTick',[]);

height = .1;%.25

grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*5/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-7.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-6.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*7/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-5.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*8/8,'Linestyle','none');
uistack(ELVOC, 'bottom');

LVOC  = rectangle('Position',[-4.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*5/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-3.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-2.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*7/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');
LVOC  = rectangle('Position',[-1.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*8/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, height+.005, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, height+.005, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.25, height+.005, 'SVOC','HorizontalAlignment','center','FontSize',16);

xlim([-9 1]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUDmassyield_color_total.pdf'],'pdf');

%% molar yield plots. be very careful in using this plot, a lot of variables are changed to produce different versions
Morg = [520; 460; 400; 350; 302; 296; 280; 264; 248];% 232]; %estimated starts at 520 for 293K, starts at 460 for 278K 
%Morg = [460; 400; 350; 302; 296; 280; 264; 248; 232]; %estimated starts at 520 for 293K, starts at 460 for 278K 
parms.ymolar = AMFVBS *168 ./ Morg;

figure
br = bar(CstarVBS,[parms.ymolar],'stack');
set(br(1),'FaceColor','green');
set(br(1),'LineWidth',2);
setFigureParameters(paperSize,'log_{10} C^* (\mu g m^{-3})','molar yield');
%set(gca,'YTickLabel',[]);
%set(gca,'YTick',[]);

height = .1;%.25;

grayLevel = 0.9;
ELVOC = rectangle('Position',[-10, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*5/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-7.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*6/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-6.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*7/8,'Linestyle','none');
uistack(ELVOC, 'bottom');
ELVOC = rectangle('Position',[-5.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 1 1]*grayLevel*8/8,'Linestyle','none');
uistack(ELVOC, 'bottom');

LVOC  = rectangle('Position',[-4.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*5/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-3.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*6/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  
LVOC  = rectangle('Position',[-2.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*7/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');
LVOC  = rectangle('Position',[-1.5, 0, 1, height],'Curvature',[0,0],...
            'FaceColor', [1 grayLevel*8/8 grayLevel],'Linestyle','none');
uistack(LVOC, 'bottom');  

SVOC  = rectangle('Position',[-0.5, 0, 2.5, height],'Curvature',[0,0],...
            'FaceColor', [grayLevel 1 grayLevel],'Linestyle','none');
uistack(SVOC, 'bottom');  
t1 = text((-8.5-4.5)/2, height+.005, 'ELVOC','HorizontalAlignment','center','FontSize',16);
t2 = text(-2.5, height+.005, 'LVOC','HorizontalAlignment','center','FontSize',16);
t3 = text(0.25, height+.005, 'SVOC','HorizontalAlignment','center','FontSize',16);

xlim([-9 1]);
set(gca,'layer','top')
saveas(gcf,['./figs/',verbos,'/distribution','/VBS_CLOUDmolaryield_color_total.pdf'],'pdf');

