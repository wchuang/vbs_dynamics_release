kwall = 0.1/60; % 1/s wall loss rate
HOMmass = 0.00790; % ug/m3
HOMmole = 2.781E-10;% mole C/m3
k_O3AP = 8e-17; % cm3/molecule/s 2nd order reaction rate
C_O3 = 35; % ppb
APppt = 242; % ppt

ppbTOmolecCm = 1e-9 / (.082*293)*6e23/1000; % ppb to molec/cm3
pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3

APmoleC = 10* APppt * pptTOmolecCm /6e23*1e6; % x10 to get moles of C in apinene
APmass = APppt * APpptTOugm3; % AP in ug/m3

massYield = (kwall * HOMmass)./ (1.8 * k_O3AP * APmass * (C_O3 *ppbTOmolecCm));
molarYield = (kwall * HOMmole)./ (1.8 *k_O3AP * APmoleC * (C_O3 *ppbTOmolecCm));

%% Corrected 
HOMmassCorr = .0193; % ug/m3
HOMmoleCorr = 7.107e-10; % mol C/m3

massYieldCorr = (kwall * HOMmassCorr)./ (1.8 * k_O3AP * APmass * (C_O3 *ppbTOmolecCm));
molarYieldCorr = (kwall * HOMmoleCorr)./(1.8 *k_O3AP * APmoleC * (C_O3 *ppbTOmolecCm));

HOMmassCorr/HOMmass
HOMmoleCorr/HOMmole

%% Building the 2D matrix, from excel file "ELVOC properties"

    %  -9     -8     -7     -6      -5      -4      -3      -2      -1       0     1     2    
    s2D = [ ...
        0      0      0      2.19e-3 .01688  0       0       0       0       0     0     0 ;  ... % 1
        0      0      0      0       0       0       .03642  0       0       0     0     0 ;  ... % .9
        0      0      0      3.58e-5 3.29e-5 0       9.47e-4 .01953  0       0     0     0 ;  ... % .8
        0      0      0      5.79e-5 3.90e-4 2.33e-4 0       2.57e-3 .09181  0     0     0 ;  ... % .7
        0      0      0      0       4.03e-5 5.54e-4 4.16e-4 0       4.92e-4 0     0     0 ;  ... % .6
        0      0      0      3.20e-4 0       2.20e-5 1.99e-4 0       6.23e-4 0     0     0 ;  ... % .5
        0      0      0      5.31e-4 0       4.17e-4 0       0       1.45e-3 0     0     0 ;  ... % .4
        0      0      0      0       0       8.31e-4 0       0       0       0     0     0 ;  ... % .3  
        0      0      0      0       0       0       0       0       0       0     0     0 ;  ... % .2  
        0      0      0      0       0       0       0       0       0       0     0     0 ;  ... % .1
        0      0      0      0       0       0       0       0       0       0     0     0 ;  ... % 0
    ];