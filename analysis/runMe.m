% This runs a simple box model of a smog chamber
clc
clear

% if a directory for figures does not exist, create it
if exist('./figs','dir') == 0, mkdir('./figs'); end;

pause off;

% setup the VBS, including yields
setupVBS;
% run a dynamical model using the yields and the VBS framework
simpleVBSDynamics;