function kcx = calcKcDp(Dp, Np)

global parms
physicalConstants;
% calculates the condensation sink based on diameter of seeds.
% used for the condensation sink weighted diameter calculation.
% assumes mass concentration of organics = 0

% calculate total volume and total mass conc
Vp = pi/6 * (Dp* nm2m)^3; % [m]
Cseed = Vp* ((Np*cm3tom3)*kgm3tougm3) * parms.rhoSeed;

% And the particle mass in amu
Mp = (Cseed)./((Np*cm3tom3))* ugtoamu;

% The reduced mass includes (monodisperse) particle mass
mu = bsxfun(@times, parms.Morg', Mp) ./ bsxfun(@plus, parms.Morg', Mp); % size(length(t), length(C*))

% In this version the speed is the center of mass collision speed 
speed = meanSpeed(mu)/4; % size(length(t), length(C*)) [m/s]

% Calc cond sink, comes up in 1/s so convert to 1/min
beta = FuchsSutugun(Dp,parms.lambda,parms.alphaOrg); % size(length(t), 1)

% Molecular enhancement (convert cross section to surface)
e_ip = (Dp+parms.Dorg).^2 ./ Dp.^2;

% The units of the frequency per area are 
%   1/time * area/volume = 1/time-length
%   here it is 1/(min-m)
freqPerArea = bsxfun(@times, speed, beta) * sec2min; % size(length(t), length(C*))
freqPerArea = bsxfun(@times, freqPerArea, e_ip); % 

% Calculate the aerosol particle surface area
surfArea = (pi*(Dp*nm2m).^2);

% Calc cond sink, comes up in 1/s so convert to 1/min
% addition of time variable requires a transposition for the dynamic
% portion of the code ( dynVBS has kc transposed when
% calculating phic_p)
kcx = bsxfun(@times, freqPerArea, surfArea .* (Np*cm3tom3) );