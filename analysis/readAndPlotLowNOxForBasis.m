%read the first set
[data.COA data.AMF] = readSOAAMF('griffin1999AMF');
p1 = semilogx(data.COA,data.AMF,'k^');
set(p1,'MarkerSize',12);
set(p1,'MarkerFaceColor',[.4 .4 .4]);

l1 = 'Griffin{\it et al.} [1999]';

data.all.COA = data.COA;
data.all.AMF = data.AMF;



hold on

[data.COA data.AMF] = readSOAAMF('cocker2001AMF');
p2 = semilogx(data.COA,data.AMF,'kd');
set(p2,'MarkerSize',12);
set(p2,'MarkerFaceColor',[.4 .4 .4]);

l2 = 'Cocker{\it et al.} [2001]';

data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];




[data.COA data.AMF] = readSOAAMF('presto2005AMF');
p3 = semilogx(data.COA,data.AMF,'ks');
set(p3,'MarkerSize',12);
set(p3,'MarkerFaceColor',[.4 .4 .4]);

l3 = 'Presto{\it et al.} [2005]';

data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];



[data.COA data.dCROG] = readSOAAPin('2005_06_14.trunc');
data.AMF = data.COA./data.dCROG;
p4 = semilogx(data.COA,data.AMF,'ko');
set(p4,'MarkerSize',12);
set(p4,'MarkerFaceColor',[.4 .4 .4]);

l4 = '14 June 2005';



data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];



[data.COA data.dCROG] = readSOAAPin('2005_06_28.trunc');
data.AMF = data.COA./data.dCROG;
p5 = semilogx(data.COA,data.AMF,'ko');
set(p5,'MarkerSize',12);
set(p5,'MarkerFaceColor',[.4 .4 .4]);

l5 = '28 June 2005';


data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];


[data.COA data.dCROG] = readSOAAPin('2005_07_13.trunc');
data.AMF = data.COA./data.dCROG;
p6 = semilogx(data.COA,data.AMF,'ko');
set(p6,'MarkerSize',12);
set(p6,'MarkerFaceColor',[.4 .4 .4]);

l6 = '13 July 2005';



data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];



[data.COA data.dCROG] = readSOAAPin('2005_07_22.trunc');
data.AMF = data.COA./data.dCROG;
p7 = semilogx(data.COA,data.AMF,'ko');
set(p7,'MarkerSize',12);
set(p7,'MarkerFaceColor',[.4 .4 .4]);

l7 = '22 July 2005';



data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];

% added by wkc 20160912
[data.COA data.AMF] = readSOAAMF('pathak2007AMF');
p3 = semilogx(data.COA,data.AMF,'ks');
set(p3,'MarkerSize',12);
set(p3,'MarkerFaceColor',[.4 .4 .4]);

l8 = 'Pathak{\it et al.} [2007]';

data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];

% added by wkc 20160912
[data.COA data.AMF] = readSOAAMF('shilling2008AMF');
p3 = semilogx(data.COA,data.AMF,'ko');
set(p3,'MarkerSize',12);
set(p3,'MarkerFaceColor',[.4 .4 .4]);

l8 = 'Shilling{\it et al.} [2008]';

data.all.COA = [data.all.COA;  data.COA];
data.all.AMF = [data.all.AMF; data.AMF];
