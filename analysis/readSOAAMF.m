function [COA AMF] = readSOAAMF(fname,path,headerlines)

if nargin < 2, path = [pwd '\data\PriorYields\']; end;
if nargin < 3, headerlines = 2; end;


[COA AMF] = textread([path fname '.dat'],'%f %f','headerlines',headerlines);

