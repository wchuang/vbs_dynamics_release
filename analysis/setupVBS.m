function setupVBS
% This function sets up all the parameters used in the program.
% The global structure called "parms" contains all of these parameters.
% setupVBSstd.m is called from this file, and contains standard parameters
% that may later be overwritten within this file.

%% Almost complete list of parameters
%{
AP - concentration of alpha-pinene [ug/m3]
APppbTOugm3 - unit conversion for alpha-pinene from [ppb] to [ug/m3]
C_O3 - concentration of ozone [ppb]
parms.chargeEffiency - This is a hypothesizes charging efficiency of the nitrate-CIMS
parms.Co - The range of volatility space
parms.constantCS - a switch for whether the condensation sink to particles is constant throughout the model run
parms.CStarShift - Shifts the yield distribution depending on the temperature
parms.DpSeed - The diameter of a monodisperse seed particle [nm]
parms.expDate - Tells the model which experimental data to use
k_O3AP - rate constant for ozone with alpha-pinene
parms.k_pw - The wall loss rate 
parms.Lprecursor - the rate of precursor reaction [ug/m3/min], for our model runs this is alpha-pinene
parms.Morg - The molecular weight of organics in each volatility bin.
parms.Np - the concentration of particles in the chamber [#/cm3]
parms.oligomerization - switch whether organics oligomerize or not
parms.origy - The yield from raw data
parms.plotVerbosity - This changes the number of plots that are produced in testPlot.m
polydisperse - a switch for whether the particle distribution is monodisperse or polydisperse.
ppbTOmolecCm - unit conversion from [ppb] to [molec/cm3]
rate - the reaction rate of alpha-pinene with ozone
parms.T - Temperature in the chamber
parms.timeEnd - The end time of the model run [min]
parms.timeStart - The start time of the model run [min]
parms.transEff - This is a hypothesized transmission efficiency of the nitrate-CIMS
verbos - A switch that determines how many plots to produce. 1 is the fewest, 3 the most. 
        This is only relevant to creating plots from mainPlots, which is not used here.
        Instead, verbos is now used as a way to save into different folders
parms.y - The yield (may be corrected) used in the model run.
%}

global parms

% 1,2,3, in increasing verbosity, only relevant to mainPlots.m, which is not currently used;
% otherwise, it is used as version numbers (e.g. 1.01,3.2,etc.)
parms.plotVerbosity = 1.0021; % 
notes = ['' '\n' ... 
    '' '\n' ...
    ''
    ];

verbos = num2str(parms.plotVerbosity); % creates a string of numbers for filename tagging later
if exist(['./figs/' verbos], 'dir') ~= 7 % if a folder with the tag does not exist, create it
    mkdir(['./figs/' verbos])
end

setupVBSstd; % default settings for the VBS that can be overridden later on. Yields and yield plots are produced here.

% see setupVBSstd.m for the default parameter settings.
% The following parameters override the ones setupVBSstd.m.

parms.T = 293; % Temperature of the chamber [K]
if parms.T == 278
    parms.CstarShift = 1e-1;
elseif parms.T == 293
    parms.CstarShift = 1;
end

%% We have some new, more discrete data on the yield distribution that we are going to try out here:
% Added 06082016 to test new yield distribution
% The data for this can be found in the file ELVOCproperties.xlsx
% This should eventually be put into setupVBS.std
if parms.CstarShift == 1e-1 % i.e. T = 278K
    parms.transEff = 1.3; % transmission efficiency of the HOMs from CLOUD
    parms.chargeEfficiency = [1;  1;  1;  1;  1/8;  1/8;  .1; .1; eps]; % This only works for CstarShift=0.1
                 % -8       -7       -6       -5        -4         -3          -2        -1         0%
    parms.origy = [.013;   .0033;   .0034;   .0058;   .0072;     .0112;       .0116;   0.0100;    .0158]; %original yields from ELVOC_properties.xls calculations   
    sum(parms.origy)
    parms.y = parms.origy ./ parms.chargeEfficiency;
    parms.y(end) = 0; parms.origy(end) = 0;
    parms.y(end-1) = 0; parms.origy(end-1) = 0;
    parms.y = parms.y * parms.transEff;
    sumYield = sum(parms.y)
    parms.Morg = [460; 395; 386; 319; 275; 274; 269; 246; 246]; % from ELVOCproperties.xlsx
    
elseif parms.CstarShift == 1 % i.e. T = 293K
    parms.transEff = 1.3;
    parms.chargeEfficiency = [1; 1;  1;  1;  1;  1/8;  1/8;  1/10; 1/10;];% 1; 1]; % This only works for CstarShift=1
                    % -8       -7       -6       -5        -4         -3        -2       -1         0%
    parms.origy = [.0086;   .0046;    .0033;   .0034;   .0058;    .0072;     .0112;    0.0116;   .0100];%original yields from ELVOC_properties.xls calculations   
    parms.origy(end) = 0;
    sum(parms.origy)
    parms.y = parms.origy ./ parms.chargeEfficiency;
    parms.y(end) = 0; 
    parms.y = parms.y * parms.transEff;
    sumYield = sum(parms.y)

    %parms.y = parms.origy; % this is to test the original distribution. Only use if prior block is commented out.
    parms.Morg = [478; 426; 395; 386; 319; 275; 274; 269; 246]; % from ELVOCproperties.xlsx    
end

%% How particle numbers change over time
parms.numTimeDependence = 'none'; % 'none' and 'time-dependent'. 
%% How vapors are lost to the walls
parms.vaporWallLossType = 'constant'; % 'constant', 'diffusion-limited to walls'
%% How vapors are lost to wall-deposited particles
parms.vaporDepositedInteraction = 'omega=0'; % 'omega=1', 'omega=0', 'diffusion-limited to walls'
%% Use the polydisperse seed input? 0:no, 1:yes
polydisperse = 0; % 0 and 1
%% Oligomerization? *** only works with polyisperse = 0
parms.oligomerization = 0; % 0 and 1    
%% *** Testing for oligomerization
if parms.oligomerization == 1
    parms.y = [.0086;   .0046;    .0033;   .0034;   .0058;    .0072;     .0112;    0.0116;   .200];
    parms.Morg = [460; 395; 386; 319; 275; 274; 269; 246; 175]; % from ELVOCproperties.xlsx
    parms.Co = [10.^(-8:-1), 1e2]';
    sum(parms.y)
end
%% Experiment parameters
parms.expDate = '20050202'; % 20051103 and 20050202 are the runs used in the paper

switch parms.expDate
    case 'testNoSeed' % only works with monodisperse, constant Np        
        parms.timeStart = 0;
        parms.timeEnd = 300;
        parms.DpSeed = 1.3;
        parms.kflow = 0; % batch reactor, no flow in/out
        parms.k_pw = 0.005; % particle loss to walls
        %parms.k_vt = parms.k_vt; % vapor loss to walls
        parms.constantCS = 0; % 1 if CS to particles is a constant, set in condSinkSimple
        
        APppbTOugm3 = 1e-9*1e9/(.082*293)*136; % ppb to ug/m3 (5.7)
        ppbTOmolecCm = 1e-9 / (.082*293)*6e23/1000; % ppb to molec/cm3 (2.4973e+010)
        
        %parms.Morg = 300; % g/mol, set so that cond sink to particle is a single number       
        parms.AP = 135*2 * APppbTOugm3; % in ppb, get ug/m3 (81=1ug/m3/hr)
        parms.C_O3 = 400 * ppbTOmolecCm; % enter in ppt, get molec/cm3 
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)
        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min       
        parms.Np = 10000; % typical concentration
    case 'testSeed' % only works with monodisperse, constant Np        
        parms.timeStart = 0;
        parms.timeEnd = 300;
        parms.DpSeed = 100;
        parms.kflow = 0; % batch reactor, no flow in/out
        parms.k_pw = 0.005; % particle loss to walls
        %parms.k_vt = parms.k_vt; % vapor loss to walls
        parms.constantCS = 0; % 1 if CS to particles is a constant, set in condSinkSimple
        
        APppbTOugm3 = 1e-9*1e9/(.082*293)*136; % ppb to ug/m3 (5.7)
        ppbTOmolecCm = 1e-9 / (.082*293)*6e23/1000; % ppb to molec/cm3 (2.4973e+010)
        
        %parms.Morg = 300; % g/mol, set so that cond sink to particle is a single number       
        parms.AP = 3 * APppbTOugm3; % in ppb, get ug/m3 (ap=3 & O3=500 gives 1ug/m3/min reac rate)
        parms.C_O3 = 5000 * ppbTOmolecCm; % enter in ppb, get molec/cm3 
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)
        printRate = parms.k_O3AP * parms.AP *1000/APppbTOugm3 * parms.C_O3 % pptv/s
        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min       
        parms.Np = 1900*10^(4/2); % seed concentration (1900 gives CSparticle=10minutes)
    %% unseeded experiments
    case '20050101' % Pathak; Temp = 293K
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 308; % in minutes, (308)        
        parms.DpSeed = 1.3; % [nm] so mult by 1e-9 to get m        
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .0081; % constant particle wall loss (.0081) [min-1]
        
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3 (.0057)
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3 (2.5e7)
        
        parms.AP = 7300 * APpptTOugm3; % enter in ppt, so in ppb, get ug/m3 (7300)
        parms.C_O3 = 1500000 * pptTOmolecCm; % enter in ppt, get molec/cm3 (1500000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)
        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 3750; % cm-3 Approximate, because particles are formed by nucleation (3750)
            case 'time-dependent'  
                parms.Np = 1; % cm-3
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 706.3; %(706.3)
                parms.initB = .09269; %(.09269)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = -.1062; %(-.1062)
                parms.levelB = 132.2; %(132.2)
                parms.levelC = 3767; %(3767)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 20; % minutes (20)
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end        
    case '20050105' % Pathak; Temp = 293K
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 361; % in minutes, (361)
        parms.DpSeed = 1.3; % [nm] so mult by 1e-9 to get m
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .006; % particle wall loss (.006) [min-1]
          
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 3700 * APpptTOugm3; % enter in ppt, so in ppb, get ug/m3 (3700)
        parms.C_O3 = 3100000 * pptTOmolecCm; % enter in ppt, get molec/cm3 (3100000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60 * parms.transEff; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 6000; % cm-3 Approximate, because particles are formed by nucleation (6000)
            case 'time-dependent'
                parms.Np = 1; % cm-3 Approximate, because particles are formed by nucleation (6000)
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 4149; %(4149)
                parms.initB = .0614; %(.0614)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = -5698; % (-5698)
                parms.levelB = 2.643; % (2.643)
                parms.levelC = 11910; % (11910)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 13.5; % minutes (13.5)
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end
    case '20050203' % Pathak; still incomplete
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 0; % in minutes, (308)        
        parms.DpSeed = 1.3; % [nm] so mult by 1e-9 to get m       
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = 0; % constant particle wall loss (.0081) [min-1]
        
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 7300 * APpptTOugm3; % enter in ppt, so in ppb, get ug/m3 (7300)
        parms.C_O3 = 1500000 * pptTOmolecCm; % enter in ppt, get molec/cm3 (1500000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 5000; % cm-3 Approximate, because particles are formed by nucleation (3750)
            case 'time-dependent'  
                parms.Np = 1; % cm-3
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 706.3; %(706.3)
                parms.initB = .09269; %(.09269)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = -.1062; %(-.1062)
                parms.levelB = 132.2; %(132.2)
                parms.levelC = 3767; %(3767)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 20; % minutes (20)
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end              
    % from Presto ES&T 2006, 14 June 2005 experiment       
    case '20041126' % Pathak; same conditions as 20050202, except without seeds, monodisperse only
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 368; % in minutes, (368)
        
        parms.DpSeed = 1.3;  % [nm] so mult by 1e-9 to get m
        parms.DpSeed1 = 1.3; % [nm] so mult by 1e-9 to get m
        parms.DpSeed2 = 1.3; % [nm] so mult by 1e-9 to get m
        parms.DpSeed3 = 1.3; % [nm] so mult by 1e-9 to get m
        
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .0072; % constant particle wall loss (.0072) [min-1]
        
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 3830 * APpptTOugm3; % enter in ppt, so in ppb, get ug/m3 (3830)
        parms.C_O3 = 250000 * pptTOmolecCm; % enter in ppt, get molec/cm3 (250000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 7100; % cm-3 Seed Concentration(7100)
            case 'time-dependent'  
                parms.Np = 1; % cm-3
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end     
        %parms.k_vt = .1 * ones(size(parms.y)); % vapor loss to teflon. 1/min
        %parms.alphaWall = 10.^(-.1919*log10(parms.Co) - 6.32);
    case '20050614' % Presto
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 340; % in minutes, (340)
        parms.DpSeed = 1.3; % [nm] so mult by 1e-9 to get m
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .0068; % particle wall loss (.0068) [min-1]
        
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3\
        
        parms.AP = 6300 * APpptTOugm3; % enter in ppt, so 6.3 ppb, get ug/m3 (6300)
        parms.C_O3 = 280000 * pptTOmolecCm; % enter in ppt, get molec/cm3 (280000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 11300; % cm-3 Approximate, because particles are formed by nucleation (11300)
            case 'time-dependent'
                parms.Np = 1;
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 1238; %(1238)
                parms.initB = .1798; %(.1798)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = -517.7; %(-517.7)
                parms.levelB = 24.3; %(24.3)
                parms.levelC = 11900; %(11900)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 11; % minutes (10)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end 
    case '20050620' % Presto, UV
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 218; % in minutes ()
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (1.3)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .0067; % particle wall loss (.0067) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 24500 * APpptTOugm3; % in ppt, so *1000 from ppb (24500)
        parms.C_O3 = 340000 * pptTOmolecCm; % ppt (340000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 17900; % cm-3 Approximate, because particles are formed by nucleation (33000)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end    
    case '20050628' % Presto
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 323; % in minutes (323)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (1.3)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .0031; % particle wall loss (.0031) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 135000 * APpptTOugm3; % in ppt, so 13.4 ppb (135000)
        parms.C_O3 = 390000 * pptTOmolecCm; % ppt (390000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 33000; % cm-3 Approximate, because particles are formed by nucleation (33000)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end        
    case '20050708' % Presto, NO2
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 280; % in minutes (280)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (1.3)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .0064; % particle wall loss (.0064) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP =  11500 * APpptTOugm3; % in ppt, so 13.4 ppb (11500)
        parms.C_O3 =  290000 * pptTOmolecCm; % ppt (290000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 490; % cm-3 Approximate, because particles are formed by nucleation (490)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end            
    case '20050713' % Presto
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 211; % in minutes (270)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (10)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .0045; % particle wall loss (.005) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 13400 * APpptTOugm3; % in ppt, so 13.4 ppb (13400)
        parms.C_O3 = 260000 * pptTOmolecCm; % ppt (260000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 7500; % cm-3 Approximate, because particles are formed by nucleation (7500)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 3419; %(3419)
                parms.initB = .1074; %(.1074)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = -7.783e-7; %(-7.783e-7)
                parms.levelB = 217.9; %(217.9)
                parms.levelC = 7536; %(7536)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 11; % minutes (11)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end
    case '20050722' % Presto
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 332; % in minutes (332)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (10)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .0037; % particle wall loss (.0037) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP =  43800 * APpptTOugm3; % in ppt, so 13.4 ppb (43800)
        parms.C_O3 =  350000 * pptTOmolecCm; % ppt (350000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 19400; % cm-3 Approximate, because particles are formed by nucleation (19400)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end       
    case '20010110b' % Cocker 2001
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 400; % in minutes (no data)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (10)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .005; % particle wall loss (no data) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP =  512500 * APpptTOugm3; % in ppt (512500)
        parms.C_O3 =  499000 * pptTOmolecCm; % ppt (499000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 32000; % cm-3 Approximate, because particles are formed by nucleation (no data)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end
    case '20010111b' % Cocker 2001
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 400; % in minutes (no data)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (10)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .005; % particle wall loss (no data) [min-1]
 
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP =  675000 * APpptTOugm3; % in ppt, so 13.4 ppb (675000)
        parms.C_O3 =  380000 * pptTOmolecCm; % ppt (380000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 35000; % cm-3 Approximate, because particles are formed by nucleation (no data)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end
    case '20010112b' % Cocker 2001
        parms.timeStart =  0; % in minutes (0)
        parms.timeEnd = 400; % in minutes (no data)
        parms.DpSeed = 1.3; % nm so mult by 1e-9 to get m (10)
        parms.kflow = 0; % batch reaction, no flow (0)
        parms.k_pw = .005; % particle wall loss (no data) [min-1]
 
        APppbTOugm3 = 1e-9*1e9/(.082*293)*136; % ppb to ug/m3
        ppbTOmolecCm = 1e-9 / (.082*293)*6e23/1000; % ppb to molec/cm3
        
        parms.AP =  686.7 * APppbTOugm3; % in ppb (686.7)
        parms.C_O3 =  258 * ppbTOmolecCm; % ppt (258)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 35000; % cm-3 Approximate, because particles are formed by nucleation (no data)
            case 'time-dependent'         
                parms.Np = 1; %      
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)   
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end
        
    %% seeded experiments
    case '20050202' % (13) Ravi's; same conditions as 20041126, except with seeds, monodisperse only, not yet ready
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 322; % in minutes, (322)
        
        parms.DpSeed = 99.6;  % [nm] so mult by 1e-9 to get m (99.6 cond-sink-weighted diamter, 127.8 mass-weighted average diameter)
        parms.rhoSeed = 1770; % [kg/m3] ammonium sulfate seeds
        parms.interactSeed = .1; % interaction term between seeds and org
        
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .0049; % constant particle wall loss (.0049) [min-1]
        
        APppbTOugm3 = 1e-9*1e9/(.082*293)*136; % ppb to ug/m3
        ppbTOmolecCm = 1e-9 / (.082*293)*6e23/1000; % ppb to molec/cm3
        
        parms.AP = 38.3 * APppbTOugm3; % in ppb, get ug/m3 (38.3)
        parms.C_O3 = 250 * ppbTOmolecCm; % enter in ppb, get molec/cm3 (250)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)
        printRate = parms.k_O3AP * parms.AP *1000/APppbTOugm3 * parms.C_O3 % pptv/s

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch polydisperse
            case 0
                parms.Np = 6000; % cm-3 Seed Concentration(6000)
            case 1
                fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Diameters.txt']);
                C = textscan(fid,'%f','HeaderLines',1);
                parms.DpSeed = C{1};
                fclose(fid);       
                fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Numbers.txt']);
                C = textscan(fid,'%f','HeaderLines',1);
                parms.Np = C{1};
                fclose(fid);
            otherwise
                error('Var:numLossType', 'Please enter valid type for number wall loss')
        end                   
    case '20050205' % Pathak ; only constant, monodisperse seed allowed
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 319; % in minutes, (319)
        
        parms.DpSeed = 193; % [nm] so mult by 1e-9 to get m
        
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .0052; % constant particle wall loss (.0081) [min-1]
        
        APpptTOugm3 = 1e-12*1e9/(.082*293)*136; % ppt to ug/m3
        pptTOmolecCm = 1e-12 / (.082*293)*6e23/1000; % ppt to molec/cm3
        
        parms.AP = 7300 * APpptTOugm3; % enter in ppt, so in ppb, get ug/m3 (7300)
        parms.C_O3 = 1500000 * pptTOmolecCm; % enter in ppt, get molec/cm3 (1500000)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch parms.numTimeDependence
            case 'none'
                parms.Np = 5000; % cm-3 Seed Concentration(5000)
            case 'time-dependent'  
                parms.Np = 1; % cm-3
                % Parameters for particle number vs time
                    % Initial burst stage, f(x) = a*(exp(b*x)-1)
                parms.initA = 0; %(--)
                parms.initB = 0; %(--)
                    % Followed by leveling off, f(x) = a*exp(b/x)+c
                parms.levelA = 0; %(--)
                parms.levelB = 0; %(--)
                parms.levelC = 0; %(--)
                    % Time at which burst changes to leveling off
                parms.timeSwitch = 0; % minutes (--)
            otherwise
                error('Var:numTimeDependence', 'Please enter valid type for number time dependence')
        end                  
    case '20051103' % (33) Pathak             
        parms.timeStart =  0; % in minutes
        parms.timeEnd = 367; % in minutes, (367)
        
        parms.DpSeed = 93.3;  % [nm] so mult by 1e-9 to get m (93.3 condsink-weighted, 112.4 mass-weighted average diameter)
        parms.rhoSeed = 1770; % [kg/m3] ammonium sulfate seeds
        parms.interactSeed = 0.0001; % interaction term between seeds and org
        
        parms.kflow = 0; % batch reaction, no flow
        parms.k_pw = .0042; % constant particle wall loss (.0042) [min-1]
        
        APppbTOugm3 = 1e-9*1e9/(.082*293)*136; % ppb to ug/m3
        ppbTOmolecCm = 1e-9 / (.082*293)*6e23/1000; % ppb to molec/cm3
        
        parms.AP = 17 * APppbTOugm3; % in ppb, get ug/m3 (17)
        parms.C_O3 = 250 * ppbTOmolecCm; % enter in ppb, get molec/cm3 (250)
        parms.k_O3AP = 8e-17; % cm3/molec/s (8e-17)
        printRate = parms.k_O3AP * parms.AP *1000/APppbTOugm3 * parms.C_O3 % pptv/s

        rate = parms.k_O3AP * (parms.AP) * (parms.C_O3); % in ug/m3/s; no factor of 1.8 because of OH scavenger
        parms.Lprecursor = rate * 60; % in ug/m3/min
        
        switch polydisperse
            case 0
                parms.Np = 12000; % cm-3 Seed Concentration(12000)
            case 1
                fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Diameters.txt']);
                C = textscan(fid,'%f','HeaderLines',1);
                parms.DpSeed = C{1};
                fclose(fid);       
                fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Numbers.txt']);
                C = textscan(fid,'%f','HeaderLines',1);
                parms.Np = C{1};
                fclose(fid);
            otherwise
                error('Var:polydisperse', 'Please enter valid type for particle number')
        end
    otherwise
        error('Var:expDate','Please enter valid date for experiment in setupVBS.m')      
end

plotYield;

%%
% text summary of the run in text file
fid = fopen(['./figs/',verbos,'/description.txt'], 'wt');
text = [[parms.expDate '\n'] ...
       ['volatility bins range C*= ' mat2str(log10(parms.Co)) '\n'] ...
       ['Temperature is ' num2str(parms.T) '\n'] ...
       ['initial alpha-pinene concentration is ' num2str(parms.AP) ' ug/m^3' '\n'] ...
       ['ozone concentration is ' num2str(parms.C_O3/ppbTOmolecCm) ' ppb' '\n'] ...
       ['seed concentration is ' num2str(parms.Np) ' cm-3' '\n'] ...
       ['yields are set to ' mat2str(sigfig(parms.y),2,[]) '\n'] ...
       ['total mass yield is ' mat2str(sigfig(sum(parms.y)),2,[]) '\n'] ...
       ['molar yields are ' mat2str(sigfig(parms.ymolar),2,[]) '\n'] ...
       ['total molar yield is ' mat2str(sigfig(sum(parms.ymolar)),2,[]) '\n'] ...
       ['time range from ' num2str(parms.timeStart) ' to ' num2str(parms.timeEnd) ' min' '\n'] ...
       ['Suspended wall loss rate is ' num2str(parms.k_pw) ' min^{-1}'] '\n' ...
       ['rate is ' num2str(printRate) ' pptv/s'] '\n' ...
       notes% '\n' ...
       ];       
   %['Seed concentration of ' num2str(parms.Np) ' cm^{-3} and starting diameter of ' num2str(parms.DpSeed) ' nm'] '\n' ...
       %['Np = ' num2str(parms.Np) ' cm^{-3}' '\n'] ...

fprintf(fid, '%s', sprintf(text));
fclose(fid);       
end