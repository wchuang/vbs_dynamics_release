global parms

[kc_prodsizes, Dp_prodsizes, freqAsizes] = ...
    condSinkSimplePolydisperse(Np_prodsizes,Cs_prodsizes,Cseed_prodsizes);

% Getting excess saturation ratio for each bin
% The effective COA includes a contribution from the seeds

Cpartsizes = Cs_prodsizes + Cseed_prodsizes*parms.interactSeed;

% The condensed phase mass fraction is the suspended concentration (Cs) 
% for each species divided by the total suspended concentration (COA);
as = bsxfun(@rdivide, Cs_prod, kron(Cpartsizes, ones(1,length(parms.y))));
as(isnan(as)) = 0 ;

% The Kelvin term
Kelvinsizes = 10.^(parms.DK ./ Dp_prodsizes);

% The condensed phase activity includes the Kelvin term
as_prime = bsxfun(@times, as, kron(Kelvinsizes, ones(1,length(parms.y))));

% The equilibrium concentration over a flat mixture
Ceq_flat = bsxfun(@times,as,repmat(parms.Co',length(t_prod),length(parms.DpSeed)));

% And finally the equilibrium concentration over the particles
Ceq = bsxfun(@times,Ceq_flat,kron(Kelvinsizes, ones(1,length(parms.y))));

% The gas-phase saturation ratio is the vapor concentration Cv divided by 
% the saturation mass concentration Co
Sv = bsxfun(@rdivide, Cv_prod, parms.Co');

% Now we can calculate the driving terms.  There are four total terms:
%  1. The saturation difference vs a flat surface (XS_sat_flat)
%  2. The saturation difference over the curved particles
%  3. The absolute difference vs a flat surface
%  4. The absolute difference

XS_sat_flat = repmat(Sv,1,numSizes) - as;
XS_sat = repmat(Sv,1,numSizes) - as_prime;

C_drive_flat = repmat(Cv_prod,1,numSizes) - Ceq_flat;
C_drive = repmat(Cv_prod,1,numSizes) - Ceq;

C_flux = bsxfun(@times,freqAsizes,C_drive);

Cfluxcells = mat2cell(C_flux, length(t_prod),repmat(numBins,numSizes,1));
C_flux_totsizes=zeros(length(t_prod),numSizes); 
for i = 1:numSizes
    C_flux_totsizes(:,i) = sum(Cscells{i},2);
end

% Calculate the number concentration
% The 1e12 converts the u and the m3 in ug/m3 to cm-3
% Take only the columns that we call HOMs with the selector
Nv = bsxfun(@rdivide,Cv_prod,repmat(parms.Morg',size(Cv_prod,1),1));
Nv = Nv * avogadroConstant / 1e12;
Nv = Nv(:,parms.selHOM);
Nv_tot = sum(Nv,2);

% The maximum flux is the flux considering vapor concentration only
% We apply a selector for the HOMs in the basis set
% Take only the columns that we call HOMs with the selector
C_flux_max = bsxfun(@times,freqAsizes,repmat(Cv_prod,1,numSizes));
C_flux_max = C_flux_max(:,repmat(parms.selHOM,1,numSizes));
C_flux_max_tot = zeros(length(t_prod),numSizes);
for i = 1:numSizes
    C_flux_max_tot(:,i) = sum(C_flux_max(i),2);
end

% the fluxes are in 1/min and the GR is nm h-1 so mult by 60
GRmult = 2 * 60 / parms.rhoOrg;
GR = bsxfun(@times,C_flux,GRmult);
GR_tot = C_flux_totsizes * GRmult;
GR_tot_max = C_flux_max_tot * GRmult;

% make a selector for the plots
plotSel = Dp_prodsizes > 0.75;