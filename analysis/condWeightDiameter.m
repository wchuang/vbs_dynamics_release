%% This function is to find the condensation-sink-weighted average diameter

clc;
clear;
global parms
physicalConstants;
setupVBS;
parms.Morg = 350; % g/mol

parms.expDate = '20051103';
rhoSeed = 1770; % [kg/m3]

% read the diameter file
fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Diameters.txt']);
C = textscan(fid,'%f','HeaderLines',1);
d_obs = C{1};
fclose(fid);

% read the number concentration file
fid = fopen(['./data/' parms.expDate '/' parms.expDate ' Seed Numbers.txt']);
C = textscan(fid,'%f','HeaderLines',1);
n_obs = C{1};
fclose(fid);

%{
d_obs = [300;400];
n_obs = [1;1];
%}
% for each size & number, calculate the contribution to condensation sink
Cseed = pi/6 * (d_obs.^3) * rhoSeed * kgm3tougm3 * nm2m^3 .* n_obs * cm3tom3; %[ug/m3]

[kc, Dp, freqPerArea] = condSinkSimple(n_obs, 0, Cseed); % calculate the cond sink, kc for each diameter

% try a summed version
[kcavg, Dpavg, freqPerAreaavg] = condSinkSimple(sum(n_obs), 0, sum(Cseed)); % calculate an overall condSink, kc

% sum the condensation sinks
kctot = sum(kc);

% backsolve with the total condensation sink and total number concentration
% to get a CS-weighted diameter.
[DpEval fEval] = fzero(@(Dpx) calcKcDp(Dpx,sum(n_obs))-kctot, 100);

CseedCheck = pi/6 * (DpEval.^3) * rhoSeed * kgm3tougm3 * nm2m^3 .* sum(n_obs) * cm3tom3;
[kcCheck, DpCheck, freqPerAreaCheck] = condSinkSimple(sum(n_obs), 0, CseedCheck);