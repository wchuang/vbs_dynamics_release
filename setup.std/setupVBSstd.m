function setupVBSstd
% This file has standard values used in the model
% setupVBS may override some of these parameters.

global parms

% This is a volatility distribution tailored to CLOUD, using
% an existing 8-bin dynamical code.  One is really, really low
% volatility to more or less seed condensation
parms.Co = [10.^(-8:2)]';           % volatility of products
parms.selHOM = parms.Co < 1;                % selector for HOMs
parms.relTol = 1e-10;                       % relative tolerance for solver
parms.absTol = 1e-13;                       % absolute tolerance for solver

parms.CstarShift = 1;%1e-1; % 10^(shift of the volatility bins)
% Charging efficiencies are not used for Albert/Ravi's calculations. Use
% the excel sheet instead to calculate yield
if parms.CstarShift == 1e-1
    parms.chargeEfficiency = [1;  1;  1;  1;  .50;  .25;  .1; .1; eps]; % This only works for CstarShift=0.1
elseif parms.CstarShift == 1
    parms.chargeEfficiency = [1; 1;  1;  1;  1;  .50;  .25;  .1; 1;];% 1; 1]; % This only works for CstarShift=1
end
parms.yieldELVOC = 0.12; % This is making the assumption that the yield of ELVOCs is 12%.

% This yields of products is then pulled from processObservations
         %  -8    -7     -6      -5     -4     -3     -2     -1      0
%parms.y = [.02; .0047; .00645; .0122; .0202; .0186; .0201;   .0154;   .00228;]; %uncorrected measured yields
%parms.y = [.02; .0047; .00645; .0122; .0202; .0186; .0201; .0154*0; .00228*0;]; %uncorrected measured yields, with no 0, -1 bins.
% We have constraints from CLOUD so we will pull the VBS from there
[parms.Co, parms.y ] = ...
    processObservations(parms.CstarShift,parms.yieldELVOC,parms.chargeEfficiency);

[parms.Dm_obs, parms.t_obs] = readDiameters;

parms.vorg = 44;                  % m/s condensation velocity

% Molecular weight g/mole of VBS, this is currently a guess
parms.Morg = [460; 400; 350; 302; 296; 280; 264; 248; 232];

%from ReadConcFile.m

parms.Dorg = 0.9;               % Diameter of a HOM monomer in nm

parms.alphaOrg = 1;               % mass accomodation coefficient
parms.lambda = 80;                % mean free path
parms.rhoOrg = 1.4e3;             % kg/m3

parms.DpSeed = 0.9;               % nm so mult by 1e-9 to get m
    % Mobility diameter is later calculated by Dpseed + interactSeed
parms.interactSeed = .75;           % interaction term between seeds and org

parms.rhoSeed = 1.4e3;            % density of seeds; kg/m3 -- assuming seed is organic too...
parms.DK = 3.75;                   % Decadal Kelvin diameter in nm

parms.Np = 1.0e3;                 % seed concentration; cm-3 so mult by 1e6 to get m-3

%% Chamber parameters

% mass accommodation coefficient, not currently used
%{
parms.chamberV = 11; % volume [m3]
%parms.k_sd = .005; % particle wall loss (.005) [min-1]
parms.alphaWall = 10.^(-.1919*log10(parms.Co) - 6.32); % wall accommodation coefficient Zhang 2015 ACP, eq. 23
parms.molecDiff = 1e-5; % [m2/s] D_i (5.3e-6?)
parms.coeffEddyDiff = .1; % [1/s] denoted as K_e in papers (33?)
parms.massOfWall = 10e6; % [ug/m3] C_w
parms.actCoeffWall = 1 * ones(size(parms.y)); % activity coefficient in the wall layer on a mole fraction basis
parms.Mw = 250; % [g/mol] average molecular weight of the absorbing organic material on the wall
%}
%% Parameters for size-dependent particle wall loss
% k = A*exp(-dp/B)+C [hr-1] , divide by 60 for [min-1]
% constant wall loss rates are set in the individual runs in setupVBS.m
parms.particleWallLossType = 'constant'; % constant or size-dependent
parms.partWallLossA = 1.05;
parms.partWallLossB = 49.973;
parms.partWallLossC = .088;

% time span of condensation
parms.timeStart = -60; % in minutes
parms.timeEnd = 270; % in minutes

% We wind up with about 0.01 ug/m3 HOMs, and they have a mass yield
% somewhere close to 0.1, meaning that you need to oxidize 0.1 ug/m3 of
% a-pinene to get 0.01 ug/m3 of HOMs. With a wall loss of order 0.1/min
% (lifetime of 10 min), the oxidation rate is about 0.01 ug/m3-min
parms.transEff = 1.3; % Empirical to get HOMs to match data
parms.Lprecursor = (0.008*1.05)*parms.transEff;        % The precursor loss rate for fcn ug/m3-min
                             
parms.precTimeCoeff = 0;           % How precursor loss rate varies with time    

parms.k_vt = .1 * ones(size(parms.y)); % vapor loss to teflon. 1/min

parms.kflow = 1/(300*60); % 3 hour timescale for CSTR timescale

parms.maxY = 0.1;                  % A scale value for maximum COA to plot
parms.maxYPart = 1e-2;             % A scale value for maximum COA to plot
parms.maxYield = 0.05;             % max yield for odum plot

parms.yLimAct = [1e-2,1e9];        % Note this needs to change with Lprec

parms.OASeedY = [.1,1e3];
parms.DiameterY = [1,3];
parms.CSY = [1e-5,.1];

parms.Dpmax = 30;                  % Max dp for linear GR figure
parms.GRmax = 40;                   % Max GR for GR figures
