clc
fid = fopen('/data/AP_O3_conc.txt');
A = fscanf(fid,'%f%f%f%f%f');
[C ending] = textscan(fid,'%f %f %f %f %f', 'HeaderLines', 1, 'Delimiter', '\t');
A = fscanf(fid,'%f%f%f%f%f');
fgetl(fid)
%[E ending2] = textscan(fid(ending+1:end), '%f %f %f %f %f', 'HeaderLines', 1, 'Delimiter', '\t');
fclose(fid);

APreact_ppt = C{1};
AP_ppt = C{2};
AP_time_matlab = C{3};
O3_ppb = C{4};
O3_time_matlab = C{5};

APtime = AP_time_matlab - AP_time_matlab(1);
O3time = O3_time_matlab - AP_time_matlab(1);

AP_time_matlab(isnan(AP_time_matlab)) =[]; % have to get rid of the NaN to do conversion
APtimeDate = datestr(AP_time_matlab);


APtime(isnan(APtime)) = [];
APreact_ppt(isnan(APreact_ppt)) = [];
AP_ppt(isnan(AP_ppt)) = [];

figure
semilogy(APtime, AP_ppt, 'Color', 'r');
hold on 
semilogy(O3time, O3_ppb, 'Color', 'b');