% This function reads the volatility and composition data from CLOUD, and
% parses the compounds into volatility bins.
% The parsing method in this code is crude, e.g. everything from -2.5 to
% -3.5 goes into C* = 10^-3. 
% A better method that is implemented in the ELVOC_properties2.xlsx parses
% the mass fractionally; e.g. half the mass from a -2.5 compound would go
% into -2, half would go into -3. This has yet to be written into a script.
clc
clear

%%
C = tdfread('/data/MassCompounds.txt');
Cstar = C.Cmod;
Comp = C.Composition;
Conc = C.Concentration_cm_3_;
MW   = C.Exactmass_Th_;

Conc(isnan(Conc)) = 0;

Cdata = [Cstar Conc MW];
Csorted = sortrows(Cdata, [-1 3]);

AvgMW = zeros(10,1);
NO3 = zeros(length(Comp),1);
HNO3 = zeros(length(Comp),1);

for i=1:length(Comp)
    NO3(i,1) = ~isempty((strfind(Comp(i,:), 'NO3') > 1)) * 62; % account for MW of NO3
    HNO3(i,1) = ~isempty((strfind(Comp(i,:), 'HNO3')>1)) * 63; % account for MW of HNO3
end

MW = MW - NO3 - HNO3; % subtract the MW of nitrate ions

for i = -7:1
    j = Cstar < i+0.5 & Cstar > i-0.5; % True for compounds that satisfy conditions
    mass_tot_i = sum(j .* Conc);
    AvgMW_i = sum(j .* Conc .* MW) / mass_tot_i;
    
    AvgMW(i+9,1) = AvgMW_i;
end

j = Cstar < -7.5;
mass_tot_i = sum(j .* Conc);
AvgMW_i = sum(j .* Conc .* MW) /mass_tot_i;
AvgMW(1,1) = AvgMW_i;
AvgMW = flipud(AvgMW);